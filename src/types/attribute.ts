export interface IAttribute
{
    htmlDocument:string,
    path:string,
    templateName:string
}
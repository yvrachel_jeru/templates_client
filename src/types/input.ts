export interface Inputs  {

    name: string,
    sqlSeverId: string,
    dbId: string,
    creationDate: Date,
    lastModifyDate: Date,
    creatorUser: string,
    lastModifyUser: string,
    headerImgPath: string,
    footerImgPath: string
  
  };
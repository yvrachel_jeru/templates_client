export default interface IMenus
{
    items:Array<IMenusItem>
}

interface IMenusItem
{
    label:string,
    items:any
}
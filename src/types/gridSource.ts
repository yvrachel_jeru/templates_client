export interface GridSource
{
    //////////////////////////////
    field:string,
    header:string,
    isEditable:boolean,
    editType?:EditType,
    dropDownValue?:any[],
    filterPlaceholder?:string,
    filterType?:FilterType
    valueGetter?: (rowData: any) => any;
}
export enum FilterType
{
    string,
    Date,
    Boolean,
    number
}
export enum EditType
{
    inputText,
    dropDown
}
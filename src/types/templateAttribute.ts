export default interface ITemplateAttribute {
  template_id: number;
  header_img_path: string;
  footer_img_path: string;
  date_format: string;
  table_style: string;
  numbering_format: string;
  numbering_place: string;
  page_orientation: string;
  is_header_footer: boolean;
  margin_bottom: number;
  margin_top: number;
  margin_left: number;
  margin_right: number;
  row_height: number;
  background_img: string;
}

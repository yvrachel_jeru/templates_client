
export  interface IOptionField
{
    template_id:number,
    ds_id: number,
    ds_field_name: string,
    ds_field_id: number,
    ds_name: string,
} 
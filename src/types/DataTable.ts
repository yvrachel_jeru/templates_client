export  interface IDataTable
{
    id:number,
    table_id: number,
    table_desc:string,
    code:number,
    combine_code_key:number,
    description:string
}
 
import { Dictionary } from "../modules/infrastructures/views/ViewsSystem";
import { KeyValuePair } from "./KeyValuePair";
import IMainFields from "./mainFields";
import { IMy_system } from "./my_system";
import { IMy_view } from "./my_view";
import { IOptionField } from "./optionField";
export  interface IMetaData
{
    fieldList:Array<IOptionField>,
    templateViewList:Array<IMy_view>,
    templateSystemList:Array<IMy_system>,
    mainFieldsList:Array<IMainFields>,
    mainFieldsValue: KeyValuePair[],
    parametersValue: KeyValuePair[]
}


import IMainField from "./mainField";

export default interface ITemplate {
    template_id?:number|null,
    system_id?:number, 
    template_name: string, 
    template_path?: string, 
    doc_name?: string, 
    status:number,
    last_modify_status:number, 
    creator_user: string, 
    last_modify_user: string, 
    main_ds_id?:number,
    arrayMF?:Array<IMainField> |null
}



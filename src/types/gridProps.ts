import { GridSource } from "./gridSource"

export default interface IGridProps
{
   data:any,
   fieldsArr:GridSource[],
   implementsEdit:Boolean,
   editFunc?:Function,
   isNavigable:boolean,
   navigate?:string,
   mainField?:string,
   jsxElement?: (data:any) => JSX.Element;

   systemName?: string
}

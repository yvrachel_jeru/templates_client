export default interface IView
{
    ds_id:number,
    ds_name:string,
    creation_date:Date,
    last_modify_date:Date,
    creator_user:string,
    last_modify_user:string,
    main_field_id:number,
    ds_parent_id:number,
    ds_parent_connection_type:number,
    parent_name_field_id:number,
    source_url:string,
    ds_type_id:number
}

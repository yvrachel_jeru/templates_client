
export interface IBody
{
    params:Array<IParams>
    storedProcedure:string
    scheme?:string
    columns?:Array<string>
    endpoint?: string; 
}

export interface IParams{
    viewId?:string
     parameterName?:string
    direction?:number
    dbType?:number
    systemName?:string
    value?:any
    isTableValue?:boolean
}
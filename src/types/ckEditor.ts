import { IOptionField } from "./optionField";
import { IOptionParam } from "./optionParam";

export interface ICkEditor {
  config: any;
  readOnly: boolean;
  type: any;
  style: any;
  name: string;
  nameFields: string;
  pressedID: number;
  viewID: number
  setFlag: any
  templateId: number
  flag: any
  stringHTML: any
  setStringHTML: any
  index: number
  setIndex: any
  content: any
  setContent: any
  templateData: any
  initContent: any
  dispatchEvent: any
  parameterOrField:boolean
  otherPressParam:boolean
  setOtherPressParam:any
  fieldId:any
  setFieldId:any
  parameterNameParent:any
  setHtmlFields?: React.Dispatch<React.SetStateAction<IOptionField[]>>
  setHtmlParam?: React.Dispatch<React.SetStateAction<IOptionParam[]>>
  view_name:string
  setJsonField:any
  setJsonParam:any
}





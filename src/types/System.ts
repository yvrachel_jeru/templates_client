export default interface ISystem
{
    system_id:number
    system_name: string,
    sqlserver_id?: number,
    db_id?: number,
    creation_date: Date,
    last_modify_date: Date,
    creator_user: string,
    last_modify_user: string,
    header_img_path: string,
    footer_img_path: string
}
 
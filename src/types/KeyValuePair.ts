export interface KeyValuePair {
    key: string|null,
    value: string|null
};
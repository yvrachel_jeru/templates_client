import { IMetaData } from "./metaData";
import ITemplateAttribute from "./templateAttribute";

export  interface IParamsForPreview
{
    templateId:number,
    metaData:IMetaData,
    html:string
    attribute?:ITemplateAttribute
}
export interface Response {
  html: string,
  result: string
}
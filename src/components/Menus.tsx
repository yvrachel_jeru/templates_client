import React from "react";
import { Menu } from 'primereact/menu';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import IMenus from "../types/menu";

export const Menus:React.FC<IMenus>=({items})=>{
  
    return(
        <div className="menus">
            <br></br>
            <Menu model={items}/>                  
        </div>
    )
}

import { Column } from "primereact/column";
import { DataTable } from "primereact/datatable";
import { useEffect, useRef, useState } from "react";
import { EditType, GridSource } from "../types/gridSource";
import 'primeicons/primeicons.css';
import React from "react";
import { InputText } from "primereact/inputtext";
import { post } from "../core/data/api";
import IGridProps from "../types/gridProps";
import { IBody, IParams } from "../types/api";
import { useNavigate, useParams } from "react-router";
import { RadioButton } from "primereact/radiobutton";
import { Dropdown } from "primereact/dropdown";
import { IDataTable } from "../types/DataTable";
import ISystem from "../types/System";
import { classNames } from "primereact/utils";
import { Button } from "primereact/button";


export default function MainGrid(props: IGridProps) {

  //--------states------------------
  const [globalFilter, setGlobalFilter] = useState(null);
  const [value, setValue] = useState();
  const [row, setRow] = useState(Object);
  const toast = useRef(null);
  const [selectedRow, setSelectedRow] = useState()
  const navigate = useNavigate();
  const [sqlServerIdArr, setSqlServerIdArr] = useState(Array<IDataTable>);
  const [dbIdArr, setDbIdArr] = useState(Array<IDataTable>);
  const [dsParentArr, setDsParentArr] = useState(Array<IDataTable>);
  const [dataType, setDataType] = useState([]);
  const { id } = useParams();
  const [globalSearch, setGlobalSearch] = useState("");
  const [fileds, setFileds] = useState(new Array<ISystem>)
  const [filteredData, setFilteredData] = useState(new Array<ISystem>);
  const [actionBodyJSX, setActionBodyJSX] = useState<JSX.Element | null>(null);
  const getServerId = async () => {
    let params: IBody = {
      params: [],
      storedProcedure: "template_dataTable_getAllSqlServer",
      scheme: "",
      columns: [
        "id",
        "table_id",
        "table_desc",
        "code",
        "combine_code_key",
        "description",
      ],
    };
    let data = await post("Data/GetData", params);
    setSqlServerIdArr(data.data[0]);
  };


  const getDbId = async () => {
    let params: IBody = {
      params: [],
      storedProcedure: "template_dataTable_getAllDb",
      scheme: "",
      columns: [
        "id",
        "table_id",
        "table_desc",
        "code",
        "combine_code_key",
        "description",
      ],
    };
    let data = await post("Data/GetData", params);
    setDbIdArr(data?.data[0]);
  };


  useEffect(() => {
    const filter = fileds.filter(item => {
      return Object.values(item).some(value => {
        const normalizedValue = String(value).toLowerCase();
        const normalizedSearch = globalSearch.toLowerCase();
        return normalizedValue.includes(normalizedSearch);
      });
    });

    setFilteredData(filter);
  }, [globalSearch, fileds]);


  const getDsParent = async () => {
    let params: IBody = {
      params: [
        {
          parameterName: "system_id",
          direction: 1,
          dbType: 1,
          value: id ? id : 0,
          isTableValue: false
        }
      ],
      storedProcedure: "template_DataSource_getAllViewName",
      scheme: "",
      columns: [
        // "value",
        // "label"
      ],
    };
    let data = await post("Data/GetData", params);
    setDsParentArr(data?.data[0]);
  };


  const getFieldType = () => {
    //כרגע זה שליפה של כל המסדי נתונים, צריך לשלוף לפי סרבר מתאים!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    const type: Array<IParams> =
      [{
        parameterName: "tableId",
        direction: 1,
        dbType: 1,
        value: "4",
        isTableValue: false
      }]
    const params: IBody =
    {
      params: type,
      storedProcedure: "templates_tablesData_get",
      scheme: "",
      columns: [""]
    }
    post("Data/GetData", params).then(data => { data && setDataType(data.data[0]) })
  };

  useEffect(() => {
    if (props.data && props.fieldsArr) {
      let data = props.data;
      setFileds(data);
    }
  }, [props.data, props.fieldsArr]);

  useEffect(() => {
    //getServerId();
    // getDbId();
    getDsParent();
    getFieldType();
  }, []);

  //--------edit---------------------
  const onRowEditInit = (event: any) => {
    setRow(event.data);
    console.log(typeof (event.data, "object"))
  }

  const onRowEditSave = () => {
    if (props.editFunc)
      props.editFunc(row.template_id)
  }

  const onRowSelect = (event: any) => {
    if (props.mainField) {
      if (props.mainField == 'ds_id')
        navigate(`${props.navigate}/${event.data.ds_id}/${event.data.system_id}`)
      if (props.mainField == 'system_id')
        navigate(`${props.navigate}/${event.data.system_id}`)
    }
  }

  const onEditorValueChange = (props1: any, value: any) => {
    let update: any = Object.assign({}, row);
    update[props1.field] = value;
    setRow(update);
  }
  const inputTextEditor = (props: any, field: any) => {
    if (row) {
      return <InputText type="text" value={row[field]} onChange={(e) => onEditorValueChange(props, e.target.value)} />;
    }
  }


  const dropDownEditor = (props: any, element: GridSource, field: any) => {
    if (row) {
      var arr: any[] = [];
      if (field == "sqlserver_id") {
        sqlServerIdArr.forEach(element => {
          arr.push(element)
        });
      }
      else {
        if (field == "db_id") {
          dbIdArr.forEach(element => {
            arr.push(element)
          });
        }
        else
          if (field == "ds_parent_id") {
            dsParentArr.forEach((element: any) => {
              arr.push(element)
            });
          }
          else {
            if (field == "field_type_id") {
              dataType.forEach((element: any) => {
                arr.push(element);
              });
            }
          }

      }
      return (field == "sqlserver_id" || field == "db_id") ? <Dropdown value={row[field]} optionLabel={'description'} optionValue={'combine_code_key'} options={arr} placeholder={row[field]} onChange={(e) => { onEditorValueChange(props, e.value); }} />
        : <Dropdown value={row[field]} options={arr} placeholder={row[field]} onChange={(e) => { onEditorValueChange(props, e.value); }} />
    }
  }

  const actionBodyTemplate = (rowData: any) => {
    if (props.mainField) {
      if (props.mainField == 'ds_id')
        return (
          <React.Fragment>
           <Button   
           label="שדות לתצוגה"
          icon="pi pi-list"
          className="p-button-success p-mr-2"
          onClick={() => { navigate(`${props.navigate}/${rowData.ds_id}/${rowData.system_id}`) }} > </Button>
          </React.Fragment>
        );
      if (props.mainField == 'system_id')
        return (
          <React.Fragment>
            <Button 
            label="תצוגות למערכת"
            icon="pi pi-list"
            className="p-button-success p-mr-2"
            style={{ width: '155px', height: '37px' }}
            onClick={() => { navigate(`${props.navigate}/${rowData.system_id}`) }} > </Button>
          </React.Fragment>
        );
    }
  }

  //--------columns--------------------

  const columns: any = props.fieldsArr.map((element: GridSource, ind: number) => {
    return element.isEditable ?
      <Column key={ind} field={element.field} header={element.header} sortable
        editor={(props) => element.editType === EditType.dropDown ? dropDownEditor(props, element, props.field) : inputTextEditor(props, props.field)} />
      : <Column key={ind} field={element.field} header={element.header} sortable />
  });
  return (
    <div>
      <div className="datatable-filter-demo">
        <div className="p-inputgroup">
          <div className="p-inputgroup">
            <Button icon="pi pi-search" className="p-button-rounded p-button-primary" />
            <InputText
              value={globalSearch}
              onChange={(e) => setGlobalSearch(e.target.value)}
              placeholder="הקלד שם לחיפוש כללי"
            />
          </div>
        </div>
        <br>
        </br>
        <br>
        </br>
        <div className="card">
          <DataTable editMode="row" className="p-datatable" value={globalSearch ? filteredData : fileds} paginator rows={10} dataKey="id" filterDisplay="row" onRowEditInit={onRowEditInit} onRowEditSave={onRowEditSave}
            globalFilter={globalFilter}
            onRowSelect={onRowSelect}
          >
            {props.isNavigable && <Column body={(data, options) => props.jsxElement && props.jsxElement(data)}></Column>}
            {columns}
            {columns}
            {props.implementsEdit && <Column header="ערוך" rowEditor headerStyle={{ width: '7rem' }} bodyStyle={{ textAlign: 'center' }} ></Column>}
          </DataTable>
        </div>
      </div>

    </div>
  );
}
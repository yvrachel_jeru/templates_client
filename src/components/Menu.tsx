import React from "react";
import { Menu } from 'primereact/menu';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

export default function Menus(){
    const items = [
        {
            label: 'Navigate',
            items: [
                {
                    label: 'c',
                    icon: 'pi pi-spin pi-cog',
                    url: '/c'
                },
                {
                    label: 'b',
                    icon: 'pi pi-search',
                    url: '/b'
                },
                {
                    label: 'a',
                    icon: 'pi pi-user',
                    url: '/a'
                }               
            ]
        }
    ]
return(
    <div className="menus">
        <Menu model={items}/>                  
    </div>
)
}

import React, { useEffect, useState } from "react";
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { InputText } from "primereact/inputtext";
import { Button } from 'primereact/button';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { IBody, IParams } from "../../../types/api";
import { post } from "../../../core/data/api";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Tag } from 'primereact/tag';
import { useParams } from "react-router";

export default function AddField(props: any) {
    const [productDialog, setProductDialog] = useState(false);
    const [submitted, setSubmitted] = useState(false);
    const [fieldName, setFieldName] = useState("");
    const [viewName, setViewName] = useState<any>();
    const [fl, setFl] = useState(true);
    const [selectedFieldType, setSelectedFieldType] = useState<any>()
    const [f2, setF2] = useState(false);
    const [dataType, setDataType] = useState([]);
    const [dataNameView, setDataNameView] = useState([""]);
    const [datafieldsToView, setDatafieldsToView] = useState<Array<any>>([]);
    const [SelectedChoose, setSelectedChoose] = useState<any>([]);
    const [tmp, setTmp] = useState<any>([]);
    const [t, setT] = useState("");
    const [ds_name, setDs_name] = useState("");
    const dataSorce_id = useParams();
    const dsId = dataSorce_id["id"]?.toString();
    console.log(dsId);

    // getAllView
    useEffect(() => {
        post("Data/GetData", nv).then(data => data && setDataNameView(data.data[0]))
    }, []);
    // getType
    useEffect(() => {
        post("Data/GetData", params).then(data => { data && setDataType(data.data[0]); })
        post("Data/GetData", ds_name_params).then(data => {
            data && console.log(data.data[0][0].ds_name, "data"); setDs_name(data.data[0][0].ds_name);
        })
    }, []);
    // getAllView
    const systemId: Array<IParams> =
        [{
            parameterName: "system_id",
            direction: 1,
            dbType: 1,
            value: props.systemId,
            isTableValue: false
        }]
    // getAllView
    const nv: IBody =
    {
        params: systemId,
        storedProcedure: "template_DataSource_getAllViewName",
        scheme: "",
        columns: [""]
    }
    // getType
    const type: Array<IParams> =
        [{
            parameterName: "tableId",
            direction: 1,
            dbType: 1,
            value: "4",
            isTableValue: false
        }]
    // getType
    const params: IBody =
    {
        params: type,
        storedProcedure: "templates_tablesData_get",
        scheme: "",
        columns: [""]
    }
    const ds_i: Array<IParams> =
        [{
            parameterName: "ds_id",
            direction: 1,
            dbType: 1,
            value: dataSorce_id["id"],
            isTableValue: false
        }]
    const ds_name_params: IBody =
    {
        params: ds_i,
        storedProcedure: "template_get_ds_name_by_id",
        scheme: "",
        columns: [""]
    }
    const leftToolbarTemplate = () => {
        return (
            <React.Fragment>
                <Button label="הוספת שדה" icon="pi pi-plus" className="p-button-success p-mr-2" onClick={() => openNew(1)} />
                <Button label="הוספת שדות מרובים" icon="pi pi-plus" className="p-button-success p-mr-2" onClick={() => openNew(2)} />
            </React.Fragment>
        )
    }
    const openNew = (num: any) => {
        setT("hi")
        setSubmitted(false);
        setProductDialog(true);
        if (num === 1)
            setFl(true)
        else
            setFl(false)

        setViewName(dsId)
        ShowFields(dsId)
    }
    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
    }
    let a: string;
    const statusEditor = (options: any) => {
        return (
            <Dropdown
                placeholder="Select a Status"
                options={dataType}
                onChange={(e: any) => {
                    const jj = () => {
                        dataType.map((rr: any) => {
                            if (rr.value == e.value) {
                                a = rr.label;
                            }
                        })
                    };
                    jj()
                    options.editorCallback(a);
                    setSelectedFieldType(e.target.value);
                }}
                itemTemplate={(option: any) => {
                    console.log(option);
                    return <Tag value={option.label} ></Tag>;
                }}
            />
        );
    };
    useEffect(() => {
        console.log("ss", SelectedChoose);
        for (let i = 0; i < tmp.length; i++) {
            if (tmp[i]?.dataType) {
                dataType.map((rr: any) => {
                    if (rr.label == SelectedChoose[i].field_0) {
                        tmp[i].dataType = rr.value;
                    }
                });
            }
        }
        saveMultiField();
    }, [tmp])

    const saveMultiField = async () => {
        console.log(tmp);
        setSubmitted(true);
        setProductDialog(false);
        console.log("t", tmp);
        console.log("s", selectedFieldType);
        // ---------------------------------------------choose(json)
        const arrParams: IParams =
        {
            parameterName: "json",
            direction: 1,
            dbType: 1,
            value: JSON.stringify(tmp),
            isTableValue: false
        }
        console.log("params" + arrParams.value);
        // ---------------------------------------------ds_id
        const ParamIdField: IParams =
        {
            parameterName: "ds_id",
            direction: 1,
            dbType: 1,
            value: viewName,
            isTableValue: false
        }
        // ---------------------------------------------creator_user
        const ParamCreator: IParams =
        {
            parameterName: "creator_user",
            direction: 1,
            dbType: 1,
            value: "D",
            isTableValue: false
        }
        // ---------------------------------------------last_modify_user
        const ParamLastModify: IParams =
        {
            parameterName: "last_modify_user",
            direction: 1,
            dbType: 1,
            value: "N",
            isTableValue: false
        }
        const params: IBody =
        {
            params: [arrParams, ParamIdField, ParamCreator, ParamLastModify],
            storedProcedure: "template_dataSourceField_multi_update_add",
            scheme: "",
            columns: [
                "ds_field_id", "ds_id", "ds_field_name", "creation_date", "last_modify_date", "creator_user", "last_modify_user", "field_type_id"]
        }
        console.log(params.params);
        await post("Data/GetData", params)
        props.func()
    }
    const saveField = async () => {
        setSubmitted(true);
        setProductDialog(false);
        let field = {
            ds_id: viewName,
            ds_field_name: fieldName,
            creation_date: new Date(),
            last_modify_date: new Date(),
            creator_user: "nomi",
            last_modify_user: "nomi",
            field_type_id: selectedFieldType,
        }
        console.log(field);
        const arrParams: Array<IParams> = [
            {
                parameterName: "json",
                direction: 1,
                dbType: 1,
                value: JSON.stringify(field),
                isTableValue: false
            },
            {
                parameterName: "ds_id",
                direction: 1,
                dbType: 1,
                value: viewName,
                isTableValue: false
            }]
        const params: IBody =
        {
            params: arrParams,
            storedProcedure: "template_dataSourceField_update_add",
            scheme: "",
            columns: [
                "ds_field_id", "ds_id", "ds_field_name", "creation_date", "last_modify_date", "creator_user", "last_modify_user", "field_type_id"]
        }
        await post("Data/GetData", params)
        props.func()
    }
    const productDialogFooter = (
        <React.Fragment>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveField} />
        </React.Fragment>
    )
    const aaa = () => {
        setTmp(SelectedChoose);
    }
    const MultiproductDialogFooter = (
        <React.Fragment>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={aaa} />
        </React.Fragment>
    )
    const ShowFields = (id: any) => {
        setF2(true)
        // get Fields to view
        const view: Array<IParams> =
            [{
                parameterName: "ds_id",
                direction: 1,
                dbType: 1,
                value: id,
                isTableValue: false
            }]
        const fieldsToView: IBody =
        {
            params: view,
            storedProcedure: "template_DataSourceField_getFieldsOfView",
            scheme: "",
            columns: [""]
        }
        post("Data/GetData", fieldsToView).then(data => {
            console.log(data.data[0]);
            setDatafieldsToView(data.data[0])
            if (data.data[0] == "" && fl == false) {
                alert("לא נמצאו שדות לתצוגה")
                setF2(false)
            }
        })
    }

    return (
        <div className="datatable-filter-demo">
            <Toolbar className="p-mb-4" left={leftToolbarTemplate}></Toolbar>
            {fl ?
                <Dialog visible={productDialog} style={{ width: '450px' }} header="הוספת שדה" modal className="p-fluid" onHide={hideDialog} footer={productDialogFooter} >
                    <div className="p-field">
                        <label htmlFor="name">field Name</label>
                        <InputText id="name" value={fieldName} onChange={(e) => setFieldName(e.target.value)} required autoFocus />
                    </div>
                    <div className="p-field">
                        <label htmlFor="viewName">view Name</label>
                        <Dropdown value={viewName} placeholder={ds_name} options={dataNameView} onChange={(e) => setViewName(e.value)} />
                    </div>
                    <div>
                        <label htmlFor="type">type</label>
                        <Dropdown value={selectedFieldType} placeholder="type" options={dataType} onChange={(e) => { setSelectedFieldType(e.value) }} />
                    </div>
                </Dialog> :
                <Dialog visible={productDialog} style={{ width: '450px' }} header="הוספת שדות מרובים" modal className="p-fluid" onHide={hideDialog} footer={MultiproductDialogFooter} >
                    <div className="p-field">
                        <div className="p-field">
                            <label htmlFor="viewName">view Name</label>
                            <Dropdown value={viewName} placeholder={ds_name} options={dataNameView} onChange={(e) => {
                                setViewName(e.value)
                                ShowFields(e.value)
                            }} />
                        </div>
                    </div>
                    {f2 &&
                        <div>
                            <br></br>
                            <DataTable value={datafieldsToView} editMode="" dataKey="columeName" selectionMode={'checkbox'} selection={SelectedChoose}
                                onSelectionChange={(e: any) => { setSelectedChoose(e.value); console.log(e.value) }}
                                tableStyle={{ minWidth: '50rem' }}>
                                <Column header="סוג שדה" editor={(options: any) => statusEditor(options)}></Column>
                                <Column field="columeName" header="שם שדה"></Column>
                                <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                            </DataTable>
                        </div>}
                </Dialog>
            }
        </div>
    );
}
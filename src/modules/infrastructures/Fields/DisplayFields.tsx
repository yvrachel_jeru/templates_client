import React, { useEffect, useState } from "react";
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import AddField from "./AddFiled";
import { FilterType, GridSource } from "../../../types/gridSource";
import { IBody, IParams } from "../../../types/api";
import IGridProps from "../../../types/gridProps";
import { post } from "../../../core/data/api";
import { useParams } from "react-router";
import MainGrid from "../../../components/grid";

export default function DisplayFields() {
    const [updated, setUpdated] = useState(false);
    const [dataItems, setdataItems] = useState();
    const [added, setAdded] = useState(false)
    const { id,systemId } = useParams();

    const arrParams: Array<IParams> =
        [{
            parameterName: "ds_id",
            direction: 1,
            dbType: 1,
            value: id,
            isTableValue: false
        }]
    const params: IBody =
    {
        params: arrParams,
        storedProcedure: "template_DataSourceField_getFields",
        scheme: "",
        columns: ["id", "nameView", "name", "updateDate", "updateUser"]
    }
    const fieldsArr: GridSource[] = [
        {
            field: "ds_field_id",
            header: "מזהה שדה",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי מזהה ",
            filterType: FilterType.number
        },
        {
            field: "ds_name",
            header: "view -שם ה",
            isEditable: true,
            filterPlaceholder: "חיפוש לפי view ",
            filterType: FilterType.string
        },
        {
            field: "ds_field_name",
            header: "שם שדה בטבלה",
            isEditable: true,
            filterPlaceholder: "Search by table Field Name",
            filterType: FilterType.string
        },
        {
            field: "last_modify_user",
            header: "שם מעדכן אחרון",
            isEditable: false,
            filterPlaceholder: "Search by last name",
            filterType: FilterType.string
        },
        {
            field: "last_modify_date",
            header: " תאריך עדכון אחרון",
            isEditable: false,
            filterPlaceholder: "Search by lastDate",
            filterType: FilterType.number
        },
    ]
    useEffect(() => {
        post("Data/GetData", params).then(data => data&&setdataItems(data.data[0]));
    }, [added]);

    const edit = (data: any) => {
        let jsonData: string
        if (data)
            jsonData = JSON.stringify(data);
        else
            jsonData = ''
        const params: IBody = {
            params: [
                {
                    parameterName: "ds_field_id",
                    direction: 1,
                    dbType: 1,
                    value: data.ds_field_id,
                    isTableValue: false
                },
                {
                    parameterName: "json",
                    direction: 1,
                    dbType: 1,
                    value: jsonData,
                    isTableValue: false
                },
            ],
            storedProcedure: "template_dataSourceField_update_add",
            scheme: "",
            columns: [
                ""
            ]
        }
        post("Data/GetData", params);
        console.log(data);
        if (updated)
            setUpdated(false);
        else
            setUpdated(true);
    }
    const propsToSend: IGridProps = {
        data: dataItems,
        fieldsArr: fieldsArr,
        implementsEdit: true,
        editFunc: edit,
        isNavigable: false
    }
    const func = () => {
        setAdded(!added)
    }
    return (
        <div>
            <div className="datatable-filter-demo">
                <div className="card">
                    <AddField func={func} systemId={systemId}></AddField>
                    <MainGrid {...propsToSend}></MainGrid>
                </div>
            </div>
        </div>
    );
}
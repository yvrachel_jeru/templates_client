export default {

    InfrastructuresMenu: [
        {
            label: 'ניתוב למערכות',
            items: [
                {
                    label: 'מערכות קיימות',
                    icon: 'pi pi-spin pi-cog',
                    url: '/infrastructures/existingSystems'
                },
                {
                    label: 'תצוגות למערכת',
                    icon: 'pi pi-search',
                    url: '/infrastructures/viewsSystem'
                },
                {
                    label: 'שדות לתצוגה',
                    icon: 'pi pi-user',
                    url: '/infrastructures/displayFields'
                },
                {
                    label: 'בקרת נתונים',
                    icon: 'pi pi-search',
                    url: '/infrastructures/dataControl'
                },
                {
                    label: 'לוג השגיאות',
                    icon: 'pi pi-user',
                    url: '/infrastructures/errorLog'
                }
            ]
        }
    ]
}
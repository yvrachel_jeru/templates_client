import React, { useEffect, useRef, useState } from "react";
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { InputText } from "primereact/inputtext";
import { Button } from 'primereact/button';
import { Toolbar } from 'primereact/toolbar';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { IBody, IParams } from "../../../types/api";
import { post } from "../../../core/data/api";
import { useParams } from "react-router";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useFormik } from 'formik';
import IView from "../../../types/view";
import { classNames } from "primereact/utils";

let showSuccess: any, setShowSuccess: any, setShowFailure: any, showFailure: any;

export default function AddView(props?: any) {

  const [viewDialog, setViewDialog] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [dsName, setDsName] = useState(''); 
  const [ParentId, setParentId] = useState(0);  
  const [sourceUrl, setSourceUrl] = useState('');
  const { id } = useParams();  
  
  [showSuccess, setShowSuccess] = useState(false);
  [showFailure, setShowFailure] = useState(false);


  const formik = useFormik({
    initialValues: {
      name:"",
    },
    validate: (values) => {
      let errors: any = {};
        // if (viewEdit?.ds_name)
        //     data.name = viewEdit?.ds_name || ' ';
        if (!values.name) {
            errors.name = 'שדה חובה!';
        }
        return errors;
    },
    onSubmit: (values) => {
      saveView(values);
    },
  });

  const leftToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button label="New" icon="pi pi-plus" className="p-button-success p-mr-2" onClick={openNew} />
      </React.Fragment>
    )
  }

  const openNew = () => {
    setSubmitted(false);
    setViewDialog(true);
  }

  const hideDialog = () => {
    setSubmitted(false);
    setViewDialog(false);
  }

  const saveView = async (values: any) => {
    setShowSuccess(true);
    if (setShowSuccess) {
      toast.success('תצוגה נוספה בהצלחה', {
        autoClose: 3000, 
        onClose: () => setShowSuccess(false),
      });
      hideDialog();
    } else {
      toast.error('Failed to add system!', {
        autoClose: false, 
        onClose: () => setShowFailure(false),
      });
    }

    let view = {
      system_id: id,
      ds_name: dsName,
      creation_date: new Date(),
      last_modify_date: new Date(),
      creator_user: 'YOEL',
      last_modify_user: 'YOEL',
      main_field_id: 1,
      ds_parent_id: ParentId,
      source_url: sourceUrl,
      ds_type_id: 1,
      parent_name_field_id: 1,
      ds_parent_connection_type: 2
    }
    let param: IParams[] = [{
      parameterName: "json",
      direction: 1,
      dbType: 1,
      value: JSON.stringify(view),
      isTableValue: false
    }, {
      parameterName: "system_id",
      direction: 1,
      dbType: 1,
      value: id,
      isTableValue: false
    }]

    let params: IBody =
    {
      params: param,
      storedProcedure: "template_view_insert_update",
      scheme: "",
      columns: [
        "ds_id",
        "ds_name",
        "creation_date",
        "last_modify_date",
        "creator_user",
        "last_modify_user",
        "main_field_id",
        "ds_parent_id",
        "ds_parent_connection_type",
        "parent_name_field_id",
        "source_url",
        "ds_type_id"]
    }
    await post("Data/GetData", params).then(data => console.log(data))
    setSubmitted(true);
    setViewDialog(false);
    setDsName('');
     props.func();
  }

  const handleButtonClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    formik.handleSubmit(e as unknown as React.FormEvent<HTMLFormElement>);
  };




  const param: IBody = {
    params: [
        {
            viewId: "setParentId",
    }  
    ],
    storedProcedure: "template_dataSourceField_getFields",
    
}
//   useEffect(() => {
//     post("Data/GetData", param).then(data => { 
        
//          console.log(ParentId+"שדות של תצוגה ") });
           
// }, [setParentId]);

  const viewDialogFooter = (
    <React.Fragment>
      <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
      <Button label="Save" icon="pi pi-check" className="p-button-text" onClick= {handleButtonClick} />
    </React.Fragment>
  );

  return (  
    // <div className="datatable-filter-demo">
    //   <ToastContainer />
    //   <Toolbar className="p-mb-4" left={leftToolbarTemplate}></Toolbar>
    //   <Dialog visible={viewDialog} style={{ width: '450px' }} header="הוספת תצוגה" modal className="p-fluid" onHide={hideDialog} footer={viewDialogFooter} >
    //     <div className="p-field">
    //       <label htmlFor="name">שם תצוגה </label>
    //       <InputText id="ds_name" value={dsName} onChange={(e) => setDsName(e.target.value)} required autoFocus />
    //     </div>
    //     <div className="p-field">
    //       <label htmlFor="name">ניתוב מקור </label>
    //       <InputText id="source_url" onChange={(e) => setSourceUrl(e.target.value)} required autoFocus />
    //     </div>
    //     <div className="p-field">
    //       <label htmlFor="ds_parent_id">תצוגת אב </label>
    //       <Dropdown optionLabel="name" optionValue="id" value={ParentId} placeholder="תצוגת אב" options={props.views} onChange={(e) => {
    //         setParentId(e.value);
    //       }}/>
    //     </div>
    //   </Dialog>
    //    {/* {validationError && <div className="error-message">{validationError}</div>}  */}
    // </div>
    <div className="datatable-filter-demo">
      <ToastContainer />
      <Toolbar className="p-mb-4" left={leftToolbarTemplate}></Toolbar>
      <Dialog visible={viewDialog} style={{ width: '450px' }} header="הוספת תצוגה" modal className="p-fluid" onHide={hideDialog} footer={viewDialogFooter} >
      <form onSubmit={formik.handleSubmit}></form>
        <div className="p-field">
          <label htmlFor="name">שם תצוגה </label>
          <InputText className={classNames({ 'p-invalid': !!(formik.touched.name && formik.errors.name) })}
          type="text"
          id="name" 
          name="name"
          //defaultValue={viewEdit && viewEdit?.ds_name}
          value={formik.values.name} 
          onChange={(e) => formik.setFieldValue('name', e.target.value)}  
          onBlur={formik.handleBlur}
          required
          autoFocus
          />  
          <br></br>
          {formik.errors.name && formik.touched.name && <div>{formik.errors.name}</div>} 
        </div>
        
        <div className="p-field">
          <label htmlFor="name">ניתוב מקור </label>
          <InputText id="source_url" onChange={(e) => setSourceUrl(e.target.value)} required autoFocus />
        </div>
        <div className="p-field">
          <label htmlFor="ds_parent_id">תצוגת אב </label>
          <Dropdown optionLabel="name" optionValue="id" value={ParentId} placeholder = "תצוגת אב" options={props.views} onChange={(e) => {
            setParentId(e.value);
          }} />
        </div>
        <div className="p-field">
          <label htmlFor="ds_parent_id">שדה הצלבה בתצוגת אב </label>
          <Dropdown optionLabel="name" optionValue="id" value={ParentId} placeholder="שדה הצלבה בתצוגת אב " options={props.views} onChange={(e) => {
            setParentId(e.value);
          }} />
        </div>
      </Dialog>
       {/* {validationError && <div className="error-message">{validationError}</div>}  */}
    </div>
  );
}
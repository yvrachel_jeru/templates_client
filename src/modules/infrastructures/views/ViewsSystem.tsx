import React, { useEffect, useState } from "react";
import AddView from "./AddView";
import { post } from "../../../core/data/api";
import { IBody } from "../../../types/api";
import { EditType, FilterType, GridSource } from "../../../types/gridSource";
import MainGrid from "../../../components/grid";
import IGridProps from "../../../types/gridProps";
import { useNavigate, useParams } from "react-router-dom";
import ISystem from "../../../types/System";
import { RadioButton } from "primereact/radiobutton";
import { Button } from 'primereact/button';

//https://www.primefaces.org/primereact-v5/#/datatable/filter
export interface Dictionary {
    id: number,
    name: string,
}

export default function ViewsSystem() {
    const [updated, setUpdated] = useState(false);
    const [dataItems, setDataItems] = useState(new Array<any>);
    const { name } = useParams(); 
    let creation_date: Date, last_modify_date: Date;
    const { id } = useParams();
    const [views, setViews] = useState(new Array<Dictionary>);
    const navigate = useNavigate();

    const param: IBody = {
        params: [
            {
                parameterName: "system_id",
                direction: 1,
                dbType: 1,
                value: id,
                isTableValue: false
        }  
        ],
        storedProcedure: "template_get_view_by_systemId",
        scheme: "",
        columns: [
            "id",
            "view_name",
            "ds_name",
            "creationDate",
            "lastModifyDate",
            "creatorUser",
            "lastModifyUser",
            "headerImgPath",
            "footerImgPath"
        ]
    }

    const fieldsArr: GridSource[] = [ 
        {
            field: "ds_id",
            header: "מזהה תצוגה",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי מזהה ",
            filterType: FilterType.number
        },
        {
            field: "ds_name",
            header: "שם תצוגה",
            isEditable: true,
            editType: EditType.inputText,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string
        },
          {
            field: "system_name",
            header: "שם מערכת",
            isEditable: true,
            editType: EditType.inputText,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string,
          },
        {
            field: "creation_date",
            header: "תאריך ייצור",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי תאריך ",
            filterType: FilterType.string
        },
        {
            field: "last_modify_date",
            header: "תאריך עדכון אחרון",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי תאריך ",
            filterType: FilterType.string
        },
        {
            field: "creator_user",
            header: "משתמש יוצר",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי יוצר ",
            filterType: FilterType.string
        },
        {
            field: "last_modify_user",
            header: "משתמש מעדכן אחרון",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string
        },
        {
            field: "ds_parent_id",
            header: "תצוגת אב",
            isEditable: true,
            editType: EditType.dropDown,
            filterPlaceholder: "חיפוש לפי תצוגת אב ",
            filterType: FilterType.string
        },
        {
            field: "source_url",
            header: "ניתוב מקור",
            isEditable: true,
            editType: EditType.inputText,
            filterPlaceholder: "חיפוש לפי נתיב ",
            filterType: FilterType.string
        },
        {
            field: "ds_type_id",
            header: "מזהה סוג",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי נתיב ",
            filterType: FilterType.number
        }
    ]
    const correctDate = (data: Array<ISystem>) => {
        if (data && Array.isArray(data)) {
        data.forEach((e: any) => { 
            const datetimeFromSql1 = e.last_modify_date;
            const datetimeFromSql2 = e.creation_date;
            const datetime1 = new Date(datetimeFromSql1);
            const datetime2 = new Date(datetimeFromSql2);
            creation_date = e.creation_date;
            last_modify_date = e.last_modify_date;
            e.last_modify_date = datetime1.toLocaleDateString();
            e.creation_date = datetime2.toLocaleDateString();
        });
    } else {
        console.log("Invalid data format");
    }
        setDataItems(data);
    }
    
    useEffect(() => {
       
        post("Data/GetData", param).then(data => { 
            setDataItems(data?.data?.[0]);
             correctDate(data?.data?.[0]);
              makeViewsList(data?.data?.[0]);
               console.log(data+"תצוגות למערכת") });
               
    }, []);

    const getData = () => {
        post("Data/GetData", param).then(data => {
            setDataItems(data?.data?.[0]); 
            correctDate(data?.data?.[0]); 
            console.log(data);
        });
    }

    const makeViewsList = (data: any[]) => {
        if (data && Array.isArray(data)) {
            let tmpViews = data.map((e) => ({
                id: e.ds_id,
                name: e.ds_name,
                system_name: e.system_name,
            }));
            setViews(tmpViews);
        } else {
            console.log("Invalid data format");
        }
    };
    

    const edit = async (data: any) => {
        let jsonData: string
        if (data) {
            data.creation_date = creation_date;
            data.last_modify_date = last_modify_date;
            jsonData = JSON.stringify(data);
            console.log(jsonData);
        }
        else
            jsonData = ''
        const params: IBody = {
            params: [
                {
                    parameterName: "ds_id",
                    direction: 1,
                    dbType: 1,
                    value: data.ds_id,
                    isTableValue: false
                },
                {
                    parameterName: "json",
                    direction: 1,
                    dbType: 1,
                    value: jsonData,
                    isTableValue: false
                },
            ],
            storedProcedure: "template_view_insert_update",
            scheme: "",
            columns: [
                ""
            ]
        }
        await post("Data/GetData", params);
        setUpdated(!updated);
        getData();
    }
    
    const actionBodyTemplate = (data: any) => {
        return (
         <React.Fragment>
           <Button   
           label="שדות לתצוגה"
          icon="pi pi-list"
          className="p-button-success p-mr-2"
          onClick={() => { navigate(`${propsToSend.navigate}/${data.system_id}`)  }} > </Button>
          </React.Fragment>
        )}

    const propsToSend: IGridProps = {
        data: dataItems,
        fieldsArr: fieldsArr,
        implementsEdit: true,
        editFunc: edit,
        isNavigable: true,
        navigate: '/infrastructures/DisplayFields',
        mainField: 'ds_id',
        jsxElement:actionBodyTemplate
        // systemName: name
    }

    const func = () => {
        getData();
    }

    return (
        <div className="datatable-filter-demo">
            <div className="card">
                <AddView func={func} views={views}></AddView>
                <MainGrid {...propsToSend}></MainGrid>
            </div>
        </div>
    );
}
import React, { useEffect, useState } from "react";
import "primereact/resources/themes/saga-blue/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Toolbar } from "primereact/toolbar";
import { Dialog } from "primereact/dialog";
import { Dropdown } from "primereact/dropdown";
import { IBody, IParams } from "../../../types/api";
import { post } from "../../../core/data/api";
import { IDataTable } from "../../../types/DataTable";
import ISystem from "../../../types/System";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useFormik } from 'formik';
import { classNames } from "primereact/utils";

let showSuccess: any, showFailure: any, setShowSuccess: any, setShowFailure: any;


export default function AddSystem(props:any) {
  [showSuccess, setShowSuccess] = useState(false);
  [showFailure, setShowFailure] = useState(false);
  const [name, setName] = useState("");
  const [sqlServerId, setSqlServerId] = useState<IDataTable>();
  const [dbId, setDbId] = useState<IDataTable>();
  const [creatorUser, setCreatorUser] = useState("");
  const [headerImgPath, setHeaderImgPath] = useState("");
  const [footerImgPath, setfooterImgPath] = useState("");
  const [showDialog, setShowDialog] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [sqlServerIdArr, setSqlServerIdArr] = useState(Array<IDataTable>);
  const [dbIdArr, setDbIdArr] = useState([]);
  const [saving, setSaving] = useState(false);
  
 
  useEffect(() => {
    console.log(props.data, "מהשליחה!!!!")
    setSqlServerIdArr(props.sqlServerIdArr)
    console.log("דרך השליחה", sqlServerIdArr)
    // getServerId();
    getDbId();
  }, []);
  const formik = useFormik({
    initialValues: {
      name: "",
      sqlServerId: null,
      dbId: null,
      creatorUser: "",
    },
    validate: (values) => {
      
      const errors: any = {};

      if (!values.name) {
        errors.name = 'שדה חובה!';
      }

      if (!values.sqlServerId) {
        errors.sqlServerId = 'שדה חובה!';
      }

      if (!values.dbId) {
        errors.dbId = 'שדה חובה!';
      }

    return errors;
    },
    onSubmit: (values) => {
      saveSystem(values);
    },
  });
  const getServerId = async () => {
    let params: IBody = {
      params: [],
      storedProcedure: "template_dataTable_getAllSqlServer",
      scheme: "",
      columns: [
        "id",
        "table_id",
        "table_desc",
        "code",
        "combine_code_key",
        "description",
      ],
    };
    let data = await post("Data/GetData", params);
    if (!data) {return}
    setSqlServerIdArr(data.data[0]);
    console.log(data.data[0], "דרך הפונקציה");
  };
  const getDbId = async () => {
    //כרגע זה שליפה של כל המסדי נתונים, צריך לשלוף לפי סרבר מתאים!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    let params: IBody = {
      params: [],
      storedProcedure: "template_dataTable_getAllDb",
      scheme: "",
      columns: [
        "id",
        "table_id",
        "table_desc",
        "code",
        "combine_code_key",
        "description",
      ],
    };
    let data = await post("Data/GetData", params);

    // Check if data and data.data are not null or undefined
    if (data?.data?.[0]) {
      setDbIdArr(data.data[0]);
    } else {
      // Handle the case where data or data.data is null or undefined
      console.log("No connection to the server or empty data");
    }
    
  };


  const [isBlue, setIsBlue] = useState(true);

  // const handleButtonClick = () => {
  //     setIsBlue(!isBlue);
  //     openNew()
  // };
  const buttonStyle = {
      border: `2px solid ${isBlue ? 'blue' : 'orange'}`,
  };


  const leftToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button
          label="הוסף"
          icon="pi pi-plus"
          style={buttonStyle}
          onClick={handleButtonClick}
        />
      </React.Fragment>
    );
  };
  const openNew = () => {
    setSubmitted(false);
    setShowDialog(true);
  };

  const hideDialog = () => {
    setSubmitted(false);
    setShowDialog(false);
  };

  const saveSystem = (values: any) => {

setShowSuccess(true);
setShowDialog(false);

if (setShowSuccess) {
  toast.success(' פרטי המערכת נשמרו בהצלחה ', {
    autoClose: 3000, 
    onClose: () => setShowSuccess(false),
  });
} else {
  toast.error('Failed to add system!', {
    autoClose: false, 
    onClose: () => setShowFailure(false),
  });
}

    let system: ISystem = {
      system_id: 0,
      system_name:  values.name,
      sqlserver_id:  values.sqlServerId?.combine_code_key,
      db_id: values.dbId?.combine_code_key,
      creation_date: new Date(),
      last_modify_date: new Date(),
      creator_user:  values.creatorUser,
      last_modify_user:  values.creatorUser,
      header_img_path: "",
      footer_img_path: "",
    };

    let param: IParams = {
      parameterName: "json",
      direction: 1,
      dbType: 1,
      value: JSON.stringify(system),
      isTableValue: false,
    };

    let params: IBody = {
      params: [param],
      storedProcedure: "template_System_insert_update",
      scheme: "",
      columns: [
        "system_id",
        "system_name",
        "sqlserver_id",
        "db_id",
        "creation_date",
        "last_modify_date",
        "creator_user",
        "last_modify_user",
        "header_img_path",
        "footer_img_path",
      ],
    };
    // post("Data/GetData", params).then(() => setSubmitted(true));
    // props.func();
    post("Data/GetData", params).then(() => {
      props.func();
      setShowSuccess(true);
    });
  };
  const handleButtonClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    formik.handleSubmit(e as unknown as React.FormEvent<HTMLFormElement>);
  };
  
  const showDialogFooter = (
    <React.Fragment>
      <Button
        label="בטל"
        icon="pi pi-times"
        className="p-button-text"
        onClick={hideDialog}
      />
      <Button
        label="שמור"
        icon="pi pi-check"
        className="p-button-text"
        type="submit"
        onClick={handleButtonClick}
      />
    </React.Fragment>
  );
  return (
    <div className="datatable-filter-demo">
      <Toolbar className="p-mb-4" left={leftToolbarTemplate}></Toolbar>
      <ToastContainer />
      <Dialog
        visible={showDialog}
        style={{ width: "450px" }}
        header="הוספת מערכת"
        modal
        className="p-fluid"
        onHide={hideDialog}
        footer={showDialogFooter}
      >
         <form onSubmit={formik.handleSubmit}></form>
        <div className="p-field">
          <label htmlFor="name">שם מערכת</label>
          <InputText
              className={classNames({ 'p-invalid': !!(formik.touched.name && formik.errors.name) })}
              type="text"
              id="name" 
              name="name"
            value={formik.values.name}
            onChange={(e) => formik.setFieldValue('name', e.target.value)}
            onBlur={formik.handleBlur}
            required
            autoFocus
          />
         {formik.touched.name && formik.errors.name && <div>{formik.errors.name}</div>}  
        </div>
        <div className="p-field">
          <label htmlFor="sql-server">שרת</label>
          <Dropdown
           className={classNames({ 'p-invalid': !!(formik.touched.sqlServerId && formik.errors.sqlServerId) })}
                id="sql-server"
                name="sql-server"  
           value={formik.values.sqlServerId}
            options={sqlServerIdArr}
            onChange={(e) => formik.setFieldValue('sqlServerId', e.value)}
              onBlur={formik.handleBlur}
              optionLabel="description"
              placeholder="בחר שם שרת"
              required
          />
          {formik.touched.sqlServerId && formik.errors.sqlServerId && <div>{formik.errors.sqlServerId}</div>}
        </div>
        <div className="p-field">
          <label htmlFor="name">מסד נתונים</label>
          <Dropdown
         className={classNames({ 'p-invalid': !!(formik.touched.dbId && formik.errors.dbId) })}
         value={formik.values.dbId}
         id="dbId"
           name="dbId"
            options={dbIdArr}
            onChange={(e) => formik.setFieldValue('dbId', e.value)}
            onBlur={formik.handleBlur}
            optionLabel="description"
            placeholder="בחר מסד נתונים"
            required
          />
           {formik.touched.dbId && formik.errors.dbId && <div>{formik.errors.dbId}</div>}
        </div>
      </Dialog>
    </div> 
  );
}

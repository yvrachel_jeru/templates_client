import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { EditType, FilterType, GridSource } from "../../../types/gridSource";
import MainGrid from "../../../components/grid";
import { post } from "../../../core/data/api";
import { IBody } from "../../../types/api";
import IGridProps from "../../../types/gridProps";
import AddSystem from "./AddSystem";
import ISystem from "../../../types/System";
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext'
import { IDataTable } from "../../../types/DataTable";
import { log } from "console";
import { RadioButton } from "primereact/radiobutton";
import { Button } from "primereact/button";


export default function System() {
    const [updated, setUpdated] = useState(false);
    const [dataItems, setDataItems] = useState(new Array<ISystem>);
    const { name } = useParams(); 
    const [sqlServerIdArr, setSqlServerIdArr] = useState(Array<IDataTable>);
    const [dbIdArr, setDbIdArr] = useState(Array<IDataTable>);
    const [data1,setData]=useState(Object)
    let sqlServerArr: string[] = [];
    let DBArr: string[] = [];
    const navigate = useNavigate();

    
    const getServerId = async () => {
        let params: IBody = {
            params: [],
            storedProcedure: "template_dataTable_getAllSqlServer",
            scheme: "",
            columns: [
                "id",
                "table_id",
                "table_desc",
                "code",
                "combine_code_key",
                "description",
            ],
        };
        let data = await post("Data/GetData", params);
        if (data && data.data && Array.isArray(data.data[0])) {
        setSqlServerIdArr(data.data[0]);
        data.data[0].forEach((element: any) => {
            sqlServerArr.push(element.description)
        });
    }else{
        console.log("No connection to the server")
    }
        // try {
        //     let data = await post("Data/GetData", params);
        //     if (data && data.data && Array.isArray(data.data[0])) {
        //         setSqlServerIdArr(data.data[0]);
        //         data.data[0].forEach((element: any) => {
        //             sqlServerArr.push(element.description)
        //         });
        //     } else {
        //         console.error("Unexpected response structure:", data);
        //     }
        // } catch (error) {
        //     console.error("Error fetching data:", error);
        // }
    };
    const getDbId = async () => {
        //כרגע זה שליפה של כל המסדי נתונים, צריך לשלוף לפי סרבר מתאים!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        let params: IBody = {
            params: [],
            storedProcedure: "template_dataTable_getAllDb",
            scheme: "",
            columns: [
                "id",
                "table_id",
                "table_desc",
                "code",
                "combine_code_key",
                "description",
            ],
        };
        let data = await post("Data/GetData", params);
        if (data && data.data && Array.isArray(data.data[0])) {
        setDbIdArr(data.data[0]);
        data.data[0].forEach((element: any) => {
            DBArr.push(element.description)
        });
    }else{
        console.log("No connection to the server")
    }
        
    };

    useEffect(() => {
        
        getServerId();
        getDbId();
        console.log(DBArr);
        console.log(sqlServerArr);
        console.log("name ",name);
    }, []);

    let creation_date: Date, last_modify_date: Date;

    let params: IBody =
    {
        params: [],
        storedProcedure: "template_system_getAll",
        scheme: "",
        columns: ["id", "name", "sqlSeverId", "dbId", "creationDate", "lastModifyDate", "creatorUser", "lastModifyUser", "headerImgPath", "footerImgPath"]
    }

    const fieldsArr: GridSource[] = [
        {
            field: "system_id",
            header: "מזהה מערכת",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי מזהה ",
            filterType: FilterType.number
        },
       
        {
            field: "system_name",
            header: "שם מערכת",
            isEditable: true,
            editType: EditType.inputText,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string,
            
        },
        {
            field: "sqlserver_id",
            header: "שרת sql",
            isEditable: true,
            editType: EditType.dropDown,
            dropDownValue: sqlServerArr,
            filterPlaceholder: "חיפוש לפי שרת ",
            filterType: FilterType.string
        },
        {
            field: "db_id",
            header: "מסד נתונים",
            isEditable: true,
            editType: EditType.dropDown,
            dropDownValue: DBArr,
            filterPlaceholder: "חיפוש לפי מסד נתונים ",
            filterType: FilterType.string
        },
        {
            field: "creation_date",
            header: " תאריך יצור",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי תאריך יצור ",
            filterType: FilterType.string
        },
        // {
        //     field: "last_modify_user",
        //     header: "משתמש יוצר",
        //     isEditable: false,
        //     filterPlaceholder: "חיפוש לפי יוצר ",
        //     filterType: FilterType.number
        // },
        {
            field: "last_modify_date",
            header: "תאריך עדכון אחרון",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי תאריך עדכון אחרון ",
            filterType: FilterType.string
        },
        {
            field: "last_modify_user",
            header: "משתמש מעדכן אחרון",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי מעכן אחרון ",
            filterType: FilterType.string
        },
        {
            field: "header_img_path",
            header: "כותרת עליונה",
            isEditable: true,
            filterPlaceholder: "חיפוש לפי כותרת עליונה ",
            filterType: FilterType.string
        },
        {
            field: "footer_img_path",
            header: "כותרת תחתונה",
            isEditable: true,
            filterPlaceholder: "חיפוש לפי כותרת תחתונה ",
            filterType: FilterType.string
        }
        ,
        {
            field: "templates",
            header: "תבניות",
            isEditable:false,
            filterPlaceholder: "חיפוש לפי תבנית ",
            filterType: FilterType.number
        }
    ]

    const correctDate = (data: Array<ISystem>) => {
        data.forEach((e: any) => {
            const datetimeFromSql1 = e.last_modify_date;
            const datetimeFromSql2 = e.creation_date;
            const datetime1 = new Date(datetimeFromSql1);
            const datetime2 = new Date(datetimeFromSql2);
            creation_date = e.creation_date;
            last_modify_date = e.last_modify_date;
            e.last_modify_date = datetime1.toLocaleDateString();
            e.creation_date = datetime2.toLocaleDateString();
        });    
        setDataItems(data)
      
    }

    useEffect(() => {
        post("Data/GetData", params)
            .then(data => {
                if (data && data.data && Array.isArray(data.data[0])) {
                    setDataItems(data.data[0]);
                    correctDate(data.data[0]);
                } else {
                    console.log("No data or invalid data structure");
                }
            })
            .catch(error => {
                console.error("Error fetching data:", error);
            });
    
        console.log(propsToSend);
        console.log("name " + name);
    }, [updated]);
    

    const edit = async (data: any) => {
        let jsonData: string
        if (data) {
            data.creation_date = creation_date;
            data.last_modify_date = last_modify_date;
            jsonData = JSON.stringify(data);
            console.log(jsonData);
            
        }
        else
            jsonData = ''
        const params: IBody = {
            params: [
                {
                    parameterName: "system_id",
                    direction: 1,
                    dbType: 1,
                    value: data.system_id,
                    isTableValue: false
                },
                {
                    parameterName: "json",
                    direction: 1,
                    dbType: 1,
                    value: jsonData,
                    isTableValue: false
                },
            ],
            storedProcedure: "template_System_insert_update",
            scheme: "",
            columns: [
                ""
            ]
        }
        post("Data/GetData", params);
        console.log('update');
        setUpdated(!updated);
    }

    
    const actionBody = (data:any) => {
        return (
   
            
    
        <React.Fragment>
            <Button 
            label="תצוגות למערכת"
            icon="pi pi-list"
            className="p-button-success p-mr-2"
            style={{ width: '155px', height: '37px' }}
            onClick={() => { navigate(`${propsToSend.navigate}/${data.system_id}`) }} > </Button>
          </React.Fragment>
        );
      };

    const func = () => {
        setUpdated(!updated)
    }

  
    const propsToSend: IGridProps = {
        data: dataItems,
        fieldsArr: fieldsArr,
        implementsEdit: true,
        editFunc: edit,
        isNavigable: true,
        navigate: '/infrastructures/ViewsSystem',
        mainField: 'system_id',
        jsxElement:actionBody,
        // systemName: name
    }

  




    return (
        <div className="datatable-filter-demo">
            <h1>מערכות</h1>
            <div className="card">
            <AddSystem func={func}></AddSystem> 
            <MainGrid  {...propsToSend}></MainGrid><br></br><br></br>
            </div>
        </div>
    );
}

import { Button } from "primereact/button";
import { useEffect, useState } from "react";
import React from "react";
import TemplateBarcode from "./templateTopButtons/templateBarcode";
import TemplateDate from "./templateTopButtons/templateDate";
import TemplatePageAttributes from "./templateTopButtons/templatePageAttributes";
import TemplatePageNumber from "./templateTopButtons/templatePageNumber";
import TemplatePhoto from "./templateTopButtons/templatePhoto";
import TemplateTable from "./templateTopButtons/templateTable";
import TemplateTitlePhotos from "./templateTopButtons/templateTitlePhotos";
import { useParams } from "react-router";

export default function TemplateButtons({setAttribute, attribute}:any) {
  const [currentComponent, setCurrentComponent] = useState<any>();
  let { id } = useParams();

  useEffect(()=>{
    console.log({attribute},"attribute")
  },[attribute])


  
  return (
    <div>
      <div
        id="top buttons"
        className="card flex flex-wrap justify-content-center gap-3">
        <Button onClick={() => setCurrentComponent(<TemplateDate />)}>
          תאריך
        </Button>
        <Button onClick={() => setCurrentComponent(<TemplateTable />)}>
          עיצוב טבלה
        </Button>
        <Button onClick={() => setCurrentComponent(<TemplatePageAttributes />)}>
          הגדרות עמוד
        </Button>
        <TemplateTitlePhotos id={id} setAttribute={setAttribute} attribute={attribute}/>
        <Button onClick={() => setCurrentComponent(<TemplatePageNumber />)}>
        </Button>
        <Button onClick={() => setCurrentComponent(<TemplatePhoto />)}>
          הוספת תמונה
        </Button>
        <Button onClick={() => setCurrentComponent(<TemplateBarcode />)}>
          ברקוד
        </Button>
      </div>
      {currentComponent}
    </div>
  );
}

import { Button } from "primereact/button";
import React, { useState } from "react";
import { post } from "../../../../core/data/api";
import { IBody, IParams } from "../../../../types/api";
import ITemplateAttribute from "../../../../types/templateAttribute";

export default function TemplateUpdate(props: any) {
  const [templateId] = useState(0);
  const [headerImgPath] = useState("");
  const [footerImgPath] = useState("");
  const [dateFormat] = useState("");
  const [tableStyle] = useState("");
  const [numberingFormat] = useState("");
  const [numberingPlace] = useState("0");
  const [pageOrientation] = useState("");
  const [isHeaderFooter] = useState(false);
  const [marginBottom] = useState(0);
  const [marginTop] = useState(0);
  const [marginLeft] = useState(0);
  const [marginRight] = useState(0);
  const [rowHeight] = useState(0);
  const [backgroundIng] = useState("");

  let param: IParams[] = [
    {
      parameterName: "templateId",
      direction: 1,
      dbType: 1,
      value: 2,
      isTableValue: false,
    },
  ];

  let paramsToGetTemplateAttribute: IBody = {
    params: param,
    storedProcedure: "template_get_template_attribute_by_id",
    scheme: "",
    columns: [""],
  };

  const saveTemplate = () => {
    debugger
    post("Data/GetData", paramsToGetTemplateAttribute).then((data) =>
      console.log(data.data[0])
    );

    let templateAttribute: ITemplateAttribute = {
      template_id: templateId,
      header_img_path: headerImgPath,
      footer_img_path: footerImgPath,
      date_format: dateFormat,
      table_style: tableStyle,
      numbering_format: numberingFormat,
      numbering_place: numberingPlace,
      page_orientation: pageOrientation,
      is_header_footer: isHeaderFooter,
      margin_bottom: marginBottom,
      margin_top: marginTop,
      margin_left: marginLeft,
      margin_right: marginRight,
      row_height: rowHeight,
      background_img: backgroundIng,
    };

    let params: IParams[] = [
      {
        parameterName: "template_id",
        direction: 1,
        dbType: 1,
        value: templateId,
        isTableValue: false,
      },
      {
        parameterName: "json",
        direction: 1,
        dbType: 1,
        value: templateAttribute,
        isTableValue: false,
      },
    ];

    let paramsToSetTemplateAttribute: IBody = {
      params: params,
      storedProcedure: "template_templatAttribute_insert_update",
      scheme: "",
      columns: ["template_id","header_img_path","footer_img_path","date_format","table_style","numbering_format","numbering_place",
      "page_orientation","is_header_footer","margin_bottom","margin_top","margin_left","margin_right","row_height","background_ing"]
    };

    post("Data/GetData", paramsToSetTemplateAttribute).then((data) =>
      console.log(data.data[0])
    );
  };
  return (
    <div>
      <Button onClick={() => saveTemplate()}>שמור טיוטה</Button>
    </div>
  );
}

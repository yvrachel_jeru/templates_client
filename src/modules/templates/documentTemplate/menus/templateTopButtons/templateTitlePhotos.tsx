import React, { Button } from "primereact/button";
import { OverlayPanel } from "primereact/overlaypanel";
import { useEffect, useRef, useState } from "react";
import { Checkbox } from "primereact/checkbox";
import { FileUpload } from "primereact/fileupload";
import { IBody, IParams } from "../../../../../types/api";
import { useParams } from "react-router";
import { post } from "../../../../../core/data/api";
import ITemplateAttribute from "../../../../../types/templateAttribute";

export default function TemplateTitlePhotos({ setAttribute, attribute }: any) {
  const op = useRef<OverlayPanel>(null);
  const [checked, setChecked] = useState<boolean>(false);
  const [templateAttributeData, setTemplateAttributeData] = useState<ITemplateAttribute>();
  const [header, setHeader] = useState("");
  const [footer, setFooter] = useState("");
  const[initContent,setInitContent]=useState()



  let { id } = useParams();

  useEffect(() => {
    console.log(id,"id");

    const params: Array<IParams> = [{
      parameterName: "templateId",
      direction: 1,
      dbType: 1,
      value: Number(id),
      isTableValue: false
    }
    ]
    const body: IBody = {
      params: params,
      storedProcedure: "template_templateAttribute_get",
      scheme: "",
      columns: []
    }
    post("Data/GetData", body).then((data) => { setTemplateAttributeData(data.data[0][0]); console.log(data.data[0]);console.log("aaaa") });
  }, [])

  
  const display = (e: any) => {
    console.log(op,"op");
    if (op.current != null) op.current.toggle(e);
  };

  const
    withoutPhoto = (e: any) => {
      setChecked(e);
      let tmp: any = attribute;
      tmp.is_header_footer = checked;
      setAttribute(tmp);
    };

    const saveHeaderFile = async(e: any) => {
      let header = await(saveFile(e, 1));
      setAttribute({
        ...attribute,
        header_img_path: header,
      });
    };

  const saveFooterFile = async(e: any) => {
    let footer = await(saveFile(e, 2));
    setAttribute({
      ...attribute,
      footer_img_path: footer,
    });
    console.log(attribute.footer_img_path, "footer");
  };

  const saveFile = (event: any, kind: number) => {
    // if (event.xhr.response) {
    let tmp: any = templateAttributeData;
    //   if (kind == 1)
    //     tmp.header_img_path = event.xhr.response
    //   else
    //     tmp.footer_img_path = event.xhr.response
    //   console.log(tmp);
    //   setAttribute(tmp);
    // }
    return event.xhr.response;
  };

  return (
    <div>
      <Button onClick={(e) => display(e)}>תמונת כותרת עליונה ותחתונה</Button>
      <div className="card flex justify-content-center">
        <OverlayPanel ref={op}>
          <div className="card">
            <FileUpload
              name="demo[]"
              url={"https://localhost:44384/api/Templates/SavePhoto"}
              accept="image/*"
              maxFileSize={1000000}
              onUpload={(e) => saveHeaderFile(e)}
              emptyTemplate={<p className="m-0">בחר תמונת כותרת עליונה</p>}
            />
          </div>
          <br />
          <br />
          <div className="card">
            <FileUpload
              name="demo[]"
              url={"https://localhost:44384/api/Templates/SavePhoto"}
              accept="image/*"
              maxFileSize={1000000}
              onUpload={(e) => saveFooterFile(e)}
              emptyTemplate={<p className="m-0">בחר תמונת כותרת תחתונה</p>}
            />
          </div>
          <br />
          <br />
          <div className="card flex justify-content-center">
            <Checkbox
              onChange={(e) => withoutPhoto(e.checked)}
              checked={checked}
            ></Checkbox>
            <label>ללא כותרת עליונה ותחתונה</label>
          </div>
        </OverlayPanel>
      </div>  
    </div>
  );
}








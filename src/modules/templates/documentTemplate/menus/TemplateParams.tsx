import { Button } from "primereact/button";
import { InputText } from "primereact/inputtext";
import { useEffect, useState } from "react";
import { post } from "../../../../core/data/api";
import { IBody, IParams } from "../../../../types/api";
import { useParams } from "react-router";

export default function TemplateParams(props: any) {

    const [parameterName, setParameterName] = useState<any>(null);
    const [parametersArray, setParametersArray] = useState<Array<any>>([]);
    const { id } = useParams();

    useEffect(() => {
        getParameters()
    }, [])

    let parameterArr: any = <>{parametersArray.length > 0 &&
        <table>
            <th align='center'>שם הפרמטר</th>
            {parametersArray.map((e) =>
                <>
                    <tr>
                        <td><Button size="small" severity="secondary" text onClick={() => { postParameters(e) }}>{e}</Button></td>
                    </tr>
                    <br></br>
                </>
            )}</table>
    }</>

    const parameters: IBody =
    {
        params: [{
            parameterName: "template",
            direction: 1,
            dbType: 1,
            value: id,
            isTableValue: false
        }],
        storedProcedure: "template_system_parameters",
        scheme: "",
        columns: ["parameter_name"]
    }

    const postParameters = (e: string) => {
        props.setParameterNameParent(e);
        props.setPressed(e);
        props.setParameterOrField(true);
        props.setOtherPressParam(!props.otherPressParam);
        props.setPressedID(props.pressedID + 1)
    }

    const getParameters = () => {
        post("Data/GetData", parameters).then(data => {
            let parametersArray: Array<any> = [];
            data.data[0].map((e: any) => {
                parametersArray.push(e.parameter_name);
            })
            setParametersArray(parametersArray);
        })
    }

    const addParam = () => {
        let ParamArr: Array<any> = parametersArray;
        ParamArr.push(parameterName);
        setParametersArray(ParamArr);
        setParameterName(null)
    }
    return (
        <div>
            <br></br>
            <div className="card flex justify-content-right">
                <span className="p-float-label">
                    <InputText id='parameter' type="text" value={parameterName} onChange={(e: React.ChangeEvent<HTMLInputElement>) => { ; setParameterName(e.target.value) }} />
                    <label htmlFor="username">הזן שם פרמטר ולחץ להוספה</label>
                </span><br></br>
                <Button size="small" severity="secondary" onClick={addParam}>הוסף פרמטר</Button>
            </div>
            {parameterArr}
        </div>
    )
}


import { InputText } from "primereact/inputtext";
import React, { useEffect, useState } from "react";
import { post, template } from "../../../../core/data/api";
import { IBody, IParams } from "../../../../types/api";
import { MultiSelect } from 'primereact/multiselect';
import { Checkbox } from 'primereact/checkbox';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import ISystem from "../../../../types/System";
import ITemplate from "../../../../types/template";
import IView from "../../../../types/view";
import { useNavigate, useParams } from "react-router";
import { useFormik } from 'formik';
import { classNames } from "primereact/utils";
import IMainField from "../../../../types/mainField";
import Swal from "sweetalert2";
import DocumentTemplates from "../DocumentTemplates";
import TemplateViews from "./TemplateViews";

const TemplateDetails = (props?: any) => {
    const [systemDataItems, setSystemDataItems] = useState<Array<ISystem>>();
    const [mainViewsDataItems, setMainViewsDataItems] = useState<Array<IView>>();
    const [mainFieldDataItems, setMainFieldDataItems] = useState<Array<IMainField>>();
    const [isExternalTemplate, setIsExternalTemplate] = useState<boolean>(false);
    const [selectSystemId, setSelectSystemId] = useState<number | undefined>(0);
    const [selectSystemName, setSelectSystemName] = useState<ISystem>();
    const [selectViewId, setSelectViewId] = useState<number | undefined>(0);
    const [selectViewName, setSelectViewName] = useState<IView>();
    const [arrayMainItems, setArrayMainItems] = useState<Array<IMainField> | undefined>();
    const [selectTemplateName, setSelectTemplateName] = useState<string>("");
    const [templateData, setTemplateData] = useState<any>();
    const [selectDefaultFormName, setSelectDefaultFormName] = useState<string | undefined>();
    const [templateEdit, setTemplateEdit] = useState<ITemplate>()
    const navigate = useNavigate();
    const { id } = useParams();
    //==================================
    //===========ADD_TEMPLATE===========
    //==================================
    // ---------------------System----------------------------
    const myBodySystem: IBody =
    {
        "params": [],
        "storedProcedure": "template_system_getAll",
        "scheme": "",
        "columns": [""]
    }
    // ---------------------views-----------------------------
    const myParamsViews: Array<IParams> = [{
        "parameterName": "systemId",
        "direction": 1,
        "dbType": 1,
        "value": selectSystemId,
        "isTableValue": false
    },
    ]
    const myBodyViews: IBody =
    {
        "params": myParamsViews,
        "storedProcedure": "template_get_all_view_by_system_id",
        "scheme": "",
        "columns": []
    }
    // --------------------mainFildes-----------------------------
    const myParamsMainFields: Array<IParams> = [{
        "parameterName": "dsId",
        "direction": 1,
        "dbType": 1,
        "value": selectViewId,
        "isTableValue": false
    },
    ]
    const myBodyMainFields: IBody =
    {
        "params": myParamsMainFields,
        "storedProcedure": "template_getDataSourceFileds_byDsId",
        "scheme": "",
        "columns": [""]
    }

    // -------------------add_template-------------------------

    let template: ITemplate =
    {
        "system_id": selectSystemId,
        "template_name": selectTemplateName,
        "template_path": "",
        // `M:\practicom\templates\backend\Files\template_${}.html`,
        "doc_name": selectDefaultFormName,
        "status": 1,
        "last_modify_status": 1,
        "creator_user": "tamar&nechami",
        "last_modify_user": "tamar&nechami",
        "main_ds_id": selectViewId,
        "arrayMF": arrayMainItems
    }

    let param: Array<IParams> = [{
        parameterName: "json",
        direction: 1,
        dbType: 1,
        value: JSON.stringify(template),
        isTableValue: false
    }]

    let bodyTemplate: IBody =
    {
        params: param,
        storedProcedure: "template_post",
        scheme: "",
        columns: [
            "system_id",
            "template_name",
            "template_path",
            "doc_name",
            "status",
            "last_modify_status",
            "creator_user",
            "last_modify_user",
            "main_ds_id",
            "arrayMF"
        ]
    }

    //getAllSystems:
    useEffect(() => {
        post('Data/GetData', myBodySystem).then(data => setSystemDataItems(data.data[0]));
    }, [])
    //getAllViewsBySystemId:
    useEffect(() => {
        post('Data/GetData', myBodyViews).then(data => {
            setMainViewsDataItems(data.data[0])
            console.log(data.data[0]);

        });
        templateEdit && getViewEditByTemplateId()
    }, [selectSystemId])
    //getAllMainFieldByViews:
    useEffect(() => {
        post('Data/GetData', myBodyMainFields).then(data => {
            setMainFieldDataItems(data.data[0])
        });
    }, [selectViewId])

    useEffect(() => {
        const tParams: Array<IParams> = [{
            parameterName: "templateId",
            direction: 1,
            dbType: 1,
            value: id,
            isTableValue: false
        }]

        const tBody: IBody = {
            params: tParams,
            storedProcedure: "template_getTemplateByTemplateId",
            scheme: "",
            columns: []
        }
        post('Data/GetData', tBody).then(data => { setTemplateData(data.data[0][0]); })
    }, [])

    //saveTemplate
    const saveTemplate = async () => {
        let templateE: ITemplate =
        {
            "system_id": selectSystemId, "template_name": selectTemplateName, "template_path":
                templateEdit?.template_path, "doc_name": selectDefaultFormName, "status": 1, "last_modify_status": 1, "creator_user": "tamar&nechami", "last_modify_user": "tamar&nechami", "main_ds_id": selectViewId, "arrayMF": arrayMainItems
        }
        let paramEdit: IBody = {
            params: [{
                parameterName: "template_id",
                direction: 1,
                dbType: 1,
                value: templateEdit?.template_id,
                isTableValue: false
            }, {
                parameterName: "json",
                direction: 1,
                dbType: 1,
                value: JSON.stringify(templateE),
                isTableValue: false
            }
            ],
            storedProcedure: "template_update",
            scheme: "",
            columns: [
                "system_id",
                "template_name",
                "template_path",
                "doc_name",
                "status",
                "last_modify_status",
                "creator_user",
                "last_modify_user",
                "main_ds_id",
                "arrayMF"
            ]
        }

        if (!templateEdit) {
            post('Data/GetData', bodyTemplate)
            // post('Data/GetData', bodyTemplate).then(data => (navigate(`/templates/DocumentTemplates/${(data.data[0][0].template_id)}`)))
                .then(data => {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success!',
                        text: 'פרטי התבנית נשמרו בהצלחה'
                    });                   
                    setTimeout(() => {
                        navigate(`/templates/DocumentTemplates/${data.data[0][0].template_id}`, { state: { data: 1 } }); // Navigate to the template design component
                        window.location.reload(); // Reload the page
                    }, 2000);
                })
                .catch(error => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'ארעה תקלה בשמירת התבנית נסה שנית'
                    })
                });
        }
        else
            post('Data/GetData', paramEdit)
                .then(response => {
                    const data = response.data;
                })
                .catch(error => {
                    alert("התבנית אינה נשמרה משום מה בדוק שוב!")
                });
        // history.push(`/templates/DocumentTemplates/${id}/views`); // Adjust the path as needed
    }
    //addMainFieldsToArray:
    const funcMainFields = (e: any) => {
        setArrayMainItems(e);
    }
    //==================================
    //===========EDIT_TEMPLATE==========
    //================================== 

    useEffect(() => {
        templateEdit && props.setIsComplete(false)
        getTemplateByTemplateId()
        let item = systemDataItems?.find(x => x.system_id === templateEdit?.system_id)
        setSelectSystemName(item);
        setSelectViewId(templateEdit?.main_ds_id)
        setSelectSystemId(item?.system_id)
        getMainFieldsEditByTemplateId()
    }, [props.templateId, systemDataItems])

    //---------------------template---------------------------
    const getTemplateByTemplateId = () => {
        // props.setIsComplete(false)
        const tParams: Array<IParams> = [{
            "parameterName": "templateId",
            "direction": 1,
            "dbType": 1,
            "value": props.templateId,
            "isTableValue": false
        }]

        const tBody: IBody = {
            params: tParams,
            storedProcedure: "template_getTemplateByTemplateId",
            scheme: "",
            columns: []
        }

        post('Data/GetData', tBody).then(data => {
            setTemplateEdit(data.data[0][0])

            console.log(data.data[0][0] + "???????");

        });
    }

    // ---------------------views-----------------------------
    const myParamsViewsEdit: Array<IParams> = [{
        "parameterName": "viewId",
        "direction": 1,
        "dbType": 1,
        "value": selectViewId,
        "isTableValue": false
    },
    ]

    const myBodyViewsEdit: IBody =
    {
        "params": myParamsViewsEdit,
        "storedProcedure": "get_view_by_view_id",
        "scheme": "",
        "columns": []
    }

    const getViewEditByTemplateId = () => {
        post('Data/GetData', myBodyViewsEdit).then((data) => {
            console.log(data.data[0][0]);
            setSelectViewName(data.data[0][0])

        });
    }

    // --------------------mainFildes-----------------------------
    const myParamsMainFieldsEdit: IBody = {
        "params": [{
            "parameterName": "template_id",
            "direction": 1,
            "dbType": 1,
            "value": templateEdit?.template_id,
            "isTableValue": false
        }]
        ,
        "storedProcedure": "template_getMainFieldsByTemplateId",
    }

    const getMainFieldsEditByTemplateId = () => {
        post('Data/GetData', myParamsMainFieldsEdit).then((data: any) => {
            console.log(data.data[0]);
            funcMainFields(data.data[0])
        });
    }

    const formik = useFormik({
        initialValues: {
            tamplateName: '',
            defaultTamplateName: '',
            system: '',
            view: '',
            mainFields: ''
        },
        validate: (data: any) => {
            let errors: any = {};
            if (templateEdit?.template_name)
                data.tamplateName = templateEdit?.template_name

            if (templateEdit?.doc_name)
                data.defaultTamplateName = templateEdit?.doc_name

            if (selectSystemName?.system_name)
                data.system = selectSystemName?.system_name

            if (selectViewName?.ds_name)
                data.view = selectViewName.ds_name

            if (arrayMainItems?.length)
                data.mainFields = " arrayMainItems"
            // =================================================
            if (!data.tamplateName) {
                errors.tamplateName = 'שדה חובה!';
            }
            if (!data.defaultTamplateName) {
                errors.defaultTamplateName = 'שדה חובה!';
            }
            if (!data.system) {
                errors.system = 'שדה חובה!';
            }
            if (!data.view) {
                errors.view = 'שדה חובה!';
            }
            if (!data.mainFields) {
                errors.mainFields = 'שדה חובה!';
            }
            return errors;
        },
        onSubmit: () => {
            saveTemplate();
        },
    });

    return (
        <div className="multiselect-demo">
            <div className="card">
                <form onSubmit={formik.handleSubmit}>
                    <label>שם תבנית</label>
                    <InputText
                        className={classNames({ 'p-invalid': !!(formik.touched.tamplateName && formik.errors.tamplateName) })}
                        name="tamplateName"
                        type="text"
                        defaultValue={templateEdit && templateEdit?.template_name}
                        onBlur={(e) => {
                            setSelectTemplateName(e.target.value)
                            formik.setFieldValue('tamplateName', e.target.value);
                        }}></InputText><br></br>
                    {!!formik.touched.tamplateName && formik.errors.tamplateName && <div>{formik.errors.tamplateName}</div>}

                    <label>שם מסמך ברירת מחדל</label>
                    <InputText
                        className={classNames({ 'p-invalid': !!(formik.touched.defaultTamplateName && formik.errors.defaultTamplateName) })}
                        name="defaultTamplateName"
                        type="text"
                        defaultValue={templateEdit && templateEdit?.doc_name}
                        onBlur={(e) => {
                            setSelectDefaultFormName(e.target.value)
                            formik.setFieldValue('defaultTamplateName', e.target.value);
                        }}>
                    </InputText><br></br>
                    {!!formik.touched.defaultTamplateName && formik.errors.defaultTamplateName && <div>{formik.errors.defaultTamplateName}</div>}

                    <label>שם מערכת</label><br></br>
                    <Dropdown
                        className={classNames({ 'p-invalid': !!(formik.touched.system && formik.errors.system) })}
                        name="system"
                        value={selectSystemName}
                        options={systemDataItems}
                        onChange={(e) => {
                            setSelectSystemId(e.value.system_id)
                            setSelectSystemName(e.value)
                            setArrayMainItems(undefined)
                            formik.setFieldValue('system', e.value);
                        }}
                        optionLabel="system_name"
                        filter
                        showClear
                        filterBy="system_name"
                        placeholder="בחר שם תצוגה" />
                    {!!formik.touched.system && formik.errors.system && <div>{formik.errors.system}</div>}

                    <label>שם תצוגה מרכזית</label><br></br>
                    <Dropdown
                        className={classNames({ 'p-invalid': !!(formik.touched.view && formik.errors.view) })}
                        name="view"
                        value={selectViewName}
                        options={mainViewsDataItems}
                        onChange={(e) => {
                            setSelectViewId(e.value.ds_id)
                            setSelectViewName(e.value)
                            setArrayMainItems(undefined)
                            formik.setFieldValue('view', e.value);
                        }}
                        optionLabel="ds_name"
                        filter
                        showClear
                        filterBy="ds_name"
                        placeholder="בחר שם תצוגה מרכזית" />
                    {!!formik.touched.view && formik.errors.view && <div>{formik.errors.view}</div>}

                    <label>שדות ראשיים</label><br></br>
                    <MultiSelect
                        className={classNames({ 'p-invalid': !!(formik.touched.mainFields && formik.errors.mainFields) })}
                        name="mainFields"
                        value={arrayMainItems}
                        options={mainFieldDataItems}
                        onChange={(e) => {
                            funcMainFields(e.value)
                            console.log(e.value);
                            formik.setFieldValue('mainFields', e.value);
                        }}
                        optionLabel="ds_field_name"
                        placeholder={"שדות ראשיים"}
                        filter
                    />
                    {!!formik.touched.mainFields && formik.errors.mainFields && <div>{formik.errors.mainFields}</div>}

                    <br></br>
                    <div className="p-field-checkbox">
                        <Checkbox inputId="binary" checked={isExternalTemplate} onChange={(e) => { setIsExternalTemplate(e.checked ? true : false) }} />
                        <label htmlFor="binary">תבנית חיצונית</label>
                    </div><br></br>
                    <Button label='שמירה' type="submit"  ></Button>
                </form>
            </div>
        </div>
    );
};
export default TemplateDetails;

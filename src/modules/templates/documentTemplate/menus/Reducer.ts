import { CKEditorEventAction } from "ckeditor4-react";

export default function reducer(state: any, action: any) {
    switch (action.type) {
        case "toolbar":
            return action.payload === state.toolbar
                ? state
                : {
                    ...state,
                    toolbar: action.payload,
                    config: getConfig(action.payload === "bold only"),
                    name: getUniqueName(),
                };
        case "type":
            return action.payload === state.type
                ? state
                : {
                    ...state,
                    type: action.payload,
                    name: getUniqueName(),
                };
        case "readOnly":
            return {
                ...state,
                readOnly: action.payload,
            };
        case "style":
            return {
                ...state,
                style: action.payload,
            };
        case CKEditorEventAction.change:
            const data = action.payload.editor.getData().trim();
            return {
                ...state,
                canSendFeedback: !!data,
                data
            };
        case "ready":
            const _data = action.payload;
            return {
                ...state,
                data: _data
            };
        default:
            return state;
    }
}

export function getUniqueName() {
    return Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, "")
        .substr(0, 5);
}

export function getConfig(boldOnly: any) {
    return {
        title: "CKEditor component",
        ...(boldOnly ? { toolbar: [["Bold"]] } : undefined),
    };
}

export function getStyle(style: any) {
    const common = {
        borderWidth: "1px",
        borderStyle: "solid",
    };

    switch (style) {
        case "blue":
            return {
                ...common,
                borderColor: "blue",
            };
        case "green":
            return {
                ...common,
                borderColor: "green",
            };
        default:
            return {};
    }
}

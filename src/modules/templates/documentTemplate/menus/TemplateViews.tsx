import React, {
  useEffect,
  useState,
} from "react";

import { htmlJson, getHTML, post, template, getPreview, deletePreview } from "../../../../core/data/api";
import { IBody, IParams } from "../../../../types/api";
import { Button } from "primereact/button";
import { Dropdown } from "primereact/dropdown";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import TemplateButtons from "./TemplateButtons";
import TemplateParams from "./TemplateParams";
import CKEditor from "./ckeditor/CKEditor";
import { useParams } from "react-router";
import { InputText } from "primereact/inputtext";
import idForPath from "../../../../types/idForPath";
import reducer, { getConfig, getStyle, getUniqueName } from "./Reducer";
import { IOptionField } from "../../../../types/optionField";
import SaveTemplateService from "../../../../core/services/SaveTemplateService";
import { IMy_system } from "../../../../types/my_system";
import { Dialog } from "primereact/dialog";
import { IOptionParam } from "../../../../types/optionParam";
import { IParamsForPreview } from "../../../../types/paramsForPreview";
import { IMetaData } from "../../../../types/metaData";
import { IMy_view } from "../../../../types/my_view";
import ITemplateAttribute from "../../../../types/templateAttribute";
import { Response } from "../../../../types/Response";
import { KeyValuePair } from "../../../../types/KeyValuePair";

const { useReducer } = React;

export default function TemplateViews(props: any) {
  const [dataFields, setDataFields] = useState(new Array<string>());
  const [viewName, setViewName] = useState();
  const [fieldsData, setFieldsData] = useState();
  const [selectedItem, setSelectedItem] = useState();
  const [rowData, setRowData] = useState();
  const [selectedViewId, setSelectedViewId] = useState();
  const [views, setViews] = useState();
  const [view, setView] = useState("");
  const [pressed, setPressed] = useState("");
  const [pressedID, setPressedID] = useState(0);
  const [flag, setFlag] = useState<boolean>(true);
  const [mainFields, setMainFields] = useState<Array<any>>([]);
  const [index, setIndex] = useState(0);
  const [content, setContent] = useState("");
  const [stringHTML, setStringHTML] = useState([]);
  const [templateData, setTemplateData] = useState<any>();
  const [viewId, setViewId] = useState(0);
  const [parameterNameParent, setParameterNameParent] = useState("");
  const [parameterOrField, setParameterOrField] = useState(false);
  const [otherPressParam, setOtherPressParam] = useState(false);
  const [fieldId, setFieldId] = useState();
  const [htmlFields, setHtmlFields] = useState<Array<IOptionField>>([]);
  const [htmlParam, setHtmlParam] = useState<Array<IOptionParam>>([]);
  const [mainFieldsValue, setMainFieldsValue] = useState<KeyValuePair[]>([]);
  const [viewNameOfTemp, setViewNameOfTemp] = useState("");
  const [first, setFirst] = useState(false);
  const [jsonParam, setJsonParam] = useState<Array<IOptionParam>>([]);
  const [jsonField, setJsonField] = useState<Array<IOptionField>>([]);
  const [attribute, setAttriute] = useState<ITemplateAttribute>();
  const [response, setResponse] = useState<Response>();
  const [preview, setPreview] = useState(false);
  const [moreOption, setMoreOption] = useState(false);
  const [initContent, setInitContent] = useState<string>("");
  const [second, setSecond] = useState(false);
  const [parametersArray, setParametersArray] = useState<Array<any>>([]);
  const [pdfFilePath, setPdfFilePath] = useState<string>("");
  const [numPages, setNumPages] = useState(null);




  let { id } = useParams();
  let numId: number = 0;
  if (id) numId = parseInt(id);
  let isRowSelectable = (event: any) => (event.data = flag);
  let tmpArr: string[] = new Array<string>();

  const onDocumentLoadSuccess = ({ numPages }: any) => {
    setNumPages(numPages);
  };



  useEffect(() => {
    isRowSelectable = (event: any) => (event.data = flag);
  }, [flag]);

  useEffect(() => {
    const tParams: Array<IParams> = [
      {
        parameterName: "templateId",
        direction: 1,
        dbType: 1,
        value: id,
        isTableValue: false,
      },
    ];

    const tBody: IBody = {
      params: tParams,
      storedProcedure: "template_getTemplateByTemplateId",
      scheme: "",
      columns: [],
    };

    post("Data/GetData", tBody).then((data) => {
      setTemplateData(data.data[0][0]);
    });
  }, []);

  useEffect(() => {
    console.log(parametersArray, "מצב הפרמטרים")
    post("Data/GetData", myParamsMainFieldsEdit).then((data: any) => {
      setMainFields(data.data[0]);
    });
  }, []);

  useEffect(() => {
    let view_name: string = "";
    let vv: any = views;
    if (vv != undefined) {
      vv.forEach((element: any) => {
        if (element.value == templateData.main_ds_id) {
          view_name = element.label
        }
      });
      setViewNameOfTemp(view_name);
    }
  }, [views])

  useEffect(() => {
    getViewName();
    getFields();
    const params: idForPath = {
      templateId: Number(id),
    };

    getHTML("Templates/GetHTML", params).then((data) => {
      setInitContent(data);
      dispatch({ type: "ready", payload: data });
    });
  }, []);

  useEffect(() => {
    getViews();
    post("Data/GetData", paramsFieldsByViewId).then((data) => {
      setFieldsData(data.data[0]);
    });
  }, [templateData]);

  useEffect(() => {
    setPressed(parameterNameParent);
  }, [parameterNameParent]);

  const params: IBody = {
    params: [
      {
        parameterName: "template_id",
        direction: 1,
        dbType: 1,
        value: id,
        isTableValue: false,
      },
    ],
    storedProcedure: "template_get_fields_by_template_id",
    scheme: "",
    columns: [""],
  };
  useEffect(() => {
    post("Data/GetData", paramsFieldsByViewId).then((data) =>
      setFieldsData(data.data[0])
    );
  }, [viewId]);


  const paramsViewName: IBody = {
    params: [
      {
        parameterName: "template_id",
        direction: 1,
        dbType: 1,
        value: id,
        isTableValue: false,
      },
    ],
    storedProcedure: "template_get_view_by_template_id",
    scheme: "",
    columns: [""],
  };
  const paramsFieldsByViewId: IBody = {
    params: [
      {
        parameterName: "ds_id",
        direction: 1,
        dbType: 1,
        value: templateData?.main_ds_id,
        isTableValue: false,
      },
    ],
    storedProcedure: "template_dataSourceField_getFields",
    scheme: "",
    columns: [""],
  };
  const paramsViews: IBody = {
    params: [
      {
        parameterName: "system_id",
        direction: 1,
        dbType: 1,
        value: templateData?.system_id,
        isTableValue: false,
      },
    ],
    storedProcedure: "template_DataSource_getAllViewName",
    scheme: "",
    columns: [""],
  };

  const myParamsMainFieldsEdit: IBody = {
    params: [
      {
        parameterName: "template_id",
        direction: 1,
        dbType: 1,
        value: id,
        isTableValue: false,
      },
    ],
    storedProcedure: "template_getMainFieldsByTemplateId",
  };

  const getViews = () => {
    post("Data/GetData", paramsViews).then((data) => {
      setViews(data.data[0]);
      let vv: any = views;
      if (vv != undefined) {
        vv.forEach((element: any) => {
          if (element.value == templateData.main_ds_id) {
            setViewNameOfTemp(element.label);
          }
        });
      }
    });
  };

  const getFields = () => {
    post("Data/GetData", params).then((data) => display(data.data[0]));
  };

  const getViewName = () => {
    post("Data/GetData", paramsViewName).then(
      (data) =>
        data.data[0][0] &&
        data.data[0][0].ds_name &&
        displayName(data.data[0][0].ds_name)
    );
  };

  const displayName = (name: any) => {
    setViewName(name);
  };

  const display = (data: any) => {
    data.forEach((e: any) => {
      tmpArr.push(e.ds_field_name);
    });
    setDataFields(tmpArr);
  };

  //---insert to html
  const onRowSelect = (event: any) => {
    setRowData(event.data);
  };

  const [
    { config, readOnly, type, style, toolbar, name, prop, data },
    dispatch,
  ] = useReducer(reducer, {
    config: getConfig(false),
    readOnly: false,
    type: "classic",
    style: "initial",
    toolbar: "standard",
    name: getUniqueName(),
    prop: pressed,
    data: initContent,
  });

  const saveAsDraft = async () => {
    try {
      const params: htmlJson = {
        templateId: Number(id),
        templateName: "nomi",
        htmlText: data,
      };
  
      let tmp: any = templateData;
      console.log(templateData);
      
      try {
        const data = await template("Templates/SaveHTML", params).then((data: any) => {
          console.log(data);
          if (tmp && tmp.template_path) {
            tmp.template_path = data;
            setTemplateData(tmp);
        alert("התבנית נשמרה בהצלחה");
      }
      })
  
        const paramsToUpdate: Array<IParams> = [
          { parameterName: "template_id", direction: 1, dbType: 1, value: id, isTableValue: false },
          { parameterName: "path", direction: 1, dbType: 1, value: templateData.template_path, isTableValue: false }
        ];
  
        const newTemplate: IBody = {
          params: paramsToUpdate,
          storedProcedure: "template_update_path",
          scheme: "",
          columns: []
        };
        console.log(newTemplate);
        await post('Data/GetData', newTemplate);
  
        const paramsToUpdateTemplateJson: Array<IParams> = [
          { parameterName: "template_id", direction: 1, dbType: 1, value: id, isTableValue: false },
          { parameterName: "templateJson", direction: 1, dbType: 1, value: JSON.stringify(templateData), isTableValue: false }
        ];
  
        await SaveTemplateService.saveFields([{ parameterName: "fieldJson", direction: 1, dbType: 1, value: JSON.stringify(jsonField), isTableValue: false }]);
        await SaveTemplateService.saveParameters([{ parameterName: "parameterJson", direction: 1, dbType: 1, value: JSON.stringify(jsonParam), isTableValue: false }]);
        await SaveTemplateService.savePhotoPath([{ parameterName: "attributeJson", direction: 1, dbType: 1, value: JSON.stringify(attribute), isTableValue: false }]);
        await SaveTemplateService.saveHtml(paramsToUpdateTemplateJson);
  
      } catch (error) {
        console.error("Error: The template was not saved. Reason:", error);
      }
    } catch (error) {
      console.error("Error in saving draft:", error);
    }
  };
  
  let paramsValues: KeyValuePair[] = [];

  const dis = async () => {
    try {

      let templateSystem: Array<IMy_system> = [];
      let system_obj: IMy_system = {
        system_id: templateData.system_id,
        system_name: templateData.system_name,
      };
      templateSystem.push(system_obj);
      let templateView: Array<IMy_view> = [];
      let view_obj: IMy_view = {
        ds_id: templateData.main_ds_id,
        ds_name: viewNameOfTemp,
      };
      templateView.push(view_obj);
      let paramsInputValues: any;
      paramsInputValues = document.body.getElementsByClassName("paramsInputValue");
      for (let index of paramsInputValues) {
        const p: KeyValuePair = {
          key: index.placeholder,
          value: index.value
        }
        paramsValues.push(p);
      }
      setFirst(true);
      let mt: IMetaData = {
        fieldList: htmlFields,
        templateViewList: templateView,
        templateSystemList: templateSystem,
        mainFieldsList: mainFields,
        mainFieldsValue: mainFieldsValue,
        parametersValue: paramsValues
      }


      let param: IParamsForPreview = {
        templateId: templateData.template_id,
        metaData: mt,
        html: data,
        attribute:attribute//האוביקט בו מתעדכנים הנתיבים של התמונות 
      }

      try {
        debugger
        console.log(param)
        let data = await getPreview('Templates/DataFromClient', param)
        console.log(data)
        const g = process.env.PUBLIC_URL + '/pdfs/preview.pdf'
        setPdfFilePath(g)
        setFirst(true);
      } catch (error) {
        console.error(error);
      }
    }
    catch {
      console.log("sorryyyyyy")
    }
  }

  const previewDisplay = async () => {
    if (parametersArray.length > 0) {
      setSecond(true);
    }
    else {
      dis()
    }
  };
  const showSaveWarning = () => {
    console.log("Attention: You did not save the changes, do you want to save?");
    return "שים לב: לא שמרת את השינויים, האם ברצונך לשמור?";
  };
  
  useEffect(() => {
    const handleBeforeUnload = (event: { preventDefault: () => void; returnValue: string; }) => {
      const confirmationMessage = showSaveWarning();
      if (confirmationMessage) {
        event.preventDefault();
        event.returnValue = confirmationMessage;
      }
    };
  
    // Attach the event listener
    window.addEventListener("beforeunload", handleBeforeUnload);
  
    // Clean up the event listener
    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, []);
  
  
  

  const close = async () => {
    try {
      const filePath = 'C:/Users/The user/Desktop/practicum/templates_client/public/pdfs/preview.pdf'
      const data = await deletePreview('Templates/DeletePreview', filePath)
      console.log(data)
      setFirst(false)
    }
    catch {
      console.log("error")
    }
  };

  
useEffect(()=>{
  console.log(attribute,"fromView")
},[attribute])


  return (
    <div className="card" style={{ width: '1500px' }}>
      {parametersArray.length > 0 ? (
        <Dialog
          header="הכנס פרמטרים"
          visible={second}
          style={{ width: '50vw', height: '20vm' }}
          onHide={() => setSecond(false)}
        >
          <p className="m-0">
            {jsonParam.map((a) => (
              <InputText key={a.parameter_name} className="paramsInputValue" placeholder={a.parameter_name}></InputText>
            ))}
            <Button onClick={dis}>תצוגה ראשונית</Button>
          </p>
        </Dialog>
      ) : (

        <Dialog
          header=""
          visible={first}
          style={{ width: '50vw', height: '20vm', overflow: 'scroll' }}
          onHide={() => close()}
        >
          <div>
            {(
              <iframe
                title="View PDF document"
                src={process.env.PUBLIC_URL + '/pdfs/preview.pdf' ? process.env.PUBLIC_URL + '/pdfs/preview.pdf' :process.env.PUBLIC_URL + '/Dassi.html' }
                width="100%"
                height="600"
                onError={(e) => console.error("Error loading PDF:", e)}
              >
                <object data="" width="100%" height="600">
                  <p>Sorry, the PDF could not be displayed.</p>
                </object>
              </iframe>
            )
            }
          </div>
        </Dialog>
      )}

      {/* <Dialog header="תצוגה ראשונית" visible={second} style={{ width: '50vw', height: '20vm' }} onHide={() => setSecond(false)}>
        <p className="m-0">
          {jsonParam.map((a) => {
            return (<InputText className="paramsInputValue" placeholder={a.parameter_name}></InputText>)
          })}
          <Button onClick={dis} > תצוגה</Button></p>
        <Dialog header="תצוגה ראשונית" visible={first} style={{ width: '50vw', height: '20vm' }} onHide={() => setFirst(false)}>
          <p className="m-0"></p>
          {response?.result == "success" ? <div dangerouslySetInnerHTML={{ __html: response.html }}></div>: <div> {response?.result}</div>}
        </Dialog>
      </Dialog> */}
      <br></br>
      <Button style={{ position: "relative", right: "35%" }} onClick={saveAsDraft}>שמור כטיוטה</Button>
      <Button style={{ position: "relative", right: "40%", top: "50%" }} onClick={() => setPreview(!preview)}>תצוגה מקדימה</Button>
      <Button style={{ position: "relative", right: "45%" }} onClick={() => { setMoreOption(!moreOption) }}>אפשריות נוספות</Button>
      {moreOption && <div><br></br> 

      <TemplateButtons setAttribute={setAttriute} attribute={attribute}></TemplateButtons></div>}
     
     
     
      <div style={{ position: "absolute", width: "1000px" }}>
        <section className="container">
          <div className="paper flex-grow-3">
            <CKEditor
              key={name}
              config={config}
              type={type}
              style={getStyle(style)}
              readOnly={flag}
              name={name}
              nameFields={pressed}
              pressedID={pressedID}
              viewID={viewId}
              templateId={numId}
              setFlag={setFlag}
              flag={flag}
              stringHTML={stringHTML}
              setStringHTML={setStringHTML}
              index={index}
              setIndex={setIndex}
              content={content}
              setContent={setContent}
              templateData={templateData}
              initContent={data}
              dispatchEvent={dispatch}
              parameterOrField={parameterOrField}
              parameterNameParent={parameterNameParent}
              otherPressParam={otherPressParam}
              setOtherPressParam={setOtherPressParam}
              fieldId={fieldId}
              setFieldId={setFieldId}
              setHtmlFields={setHtmlFields}
              setHtmlParam={setHtmlParam}
              view_name={viewNameOfTemp}
              setJsonField={setJsonField}
              setJsonParam={setJsonParam}
            />
          </div>
        </section>
      </div> 

      <div className="parent" style={{ position: "relative" }}>
        <div style={{ right: "85%", width: "30%", position: "absolute" }}>
          {preview &&
            <div>
              <br></br>
              <Button style={{ position: "relative", left: "30%", background: "#eebb2c" }} onClick={previewDisplay}>הצגה ראשונית</Button>
              <h2 style={{ position: "relative", left: "30%" }}>שדות ראשיים</h2>
              {mainFields.map((event, i) => {
                return (
                  <div>
                    <label style={{ position: "relative", left: "30%" }}>{event?.ds_field_name}</label>
                    <InputText style={{ position: "relative", left: "30%" }}
                      id={event.ds_field_name}
                      onChange={(e: any) => {
                        let tmp = [...mainFieldsValue];
                        let mainFieldValue: KeyValuePair = { key: event.ds_field_name, value: e.target.value }
                        tmp.push(mainFieldValue)
                        setMainFieldsValue(tmp);
                      }}
                      required
                      autoFocus
                    ></InputText>
                  </div>
                );
              })}
            </div>
          }
          <h2 style={{ position: "relative", left: "30%" }}>שדות ופרמטרים לבחירה</h2>
          <h3 style={{ position: "relative", left: "30%" }}>שדות</h3>
          <span style={{ position: "relative", left: "30%" }}>בחר את שם התצוגה</span>
          <Dropdown style={{ position: "relative", left: "30%" }}
            value={view}
            options={views}
            onChange={(e) => {
              setSelectedViewId(e.value);
              setViewId(e.value);
              setView(e.value);
            }}
            placeholder="Select a view"
          />
          <br></br><br></br>
          <DataTable style={{ position: "relative", left: "30%" }}
            paginator
            rows={7}
            isDataSelectable={isRowSelectable}
            value={fieldsData}
            selection={selectedItem}
            onSelectionChange={(e: any) => {
              setSelectedItem(e.value);
              setPressed(e.value.ds_field_name);
              setPressedID(pressedID + 1);
              setFieldId(e.value.ds_field_id);
              setViewId(e.value?.ds_id);
              setParameterOrField(false);
            }}
            selectionMode="single"
            onRowSelect={onRowSelect}
          >
            <Column field="ds_field_name" header="שמות השדות"></Column>
          </DataTable>
          <h3 style={{ position: "relative", left: "30%" }}>פרמטרים</h3>
          <div style={{ position: "relative", left: "30%" }}>
            <TemplateParams
              otherPressParam={otherPressParam}
              setPressed={setPressed}
              pressedID={pressedID}
              setPressedID={setPressedID}
              setOtherPressParam={setOtherPressParam}
              setParameterNameParent={setParameterNameParent}
              setParameterOrField={setParameterOrField}
            ></TemplateParams>
          </div>
        </div>
      </div>
    </div >
  );
}
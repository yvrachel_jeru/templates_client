import * as React from "react";
import { useEffect } from "react";

const defaultFonts = [
  "Arial",
  "Comic Sans MS",
  "Courier New",
  "Impact",
  "Georgia",
  "Tahoma",
  "Trebuchet MS",
  "Verdana"
];
export default function App(props: any) {
  const [value, setValue] = React.useState("");
  const [obj, setObj] = React.useState();
  useEffect(() => {
    console.log(props);
    setObj(props);
    setValue(JSON.stringify(tags));

  }, [])
  const sortedFontOptions = [
    "Logical",
    "Salesforce Sans",
    "Garamond",
    "Sans-Serif",
    "Serif",
    "Times New Roman",
    "Helvetica",
    ...defaultFonts
  ].sort();
  const tags: string = '<h1' + { obj } + '></h1>';

  return (

    <div className="App">
      <hr />
      <h2>Example value output:</h2>
      <textarea
        disabled
        value={JSON.stringify({ value }, null, 2)}
        style={{ width: "100%", resize: "none", height: "600px" }}
      />
    </div>
  );
}

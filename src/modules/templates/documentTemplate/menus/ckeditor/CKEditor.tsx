import * as React from "react";
import {useCKEditor } from "ckeditor4-react";
import { ICkEditor } from "../../../../../types/ckEditor";
import { useParams } from "react-router";
import { Button } from "primereact/button";
import { Dialog } from "primereact/dialog";
import { IOptionParam } from "../../../../../types/optionParam";
import { IOptionField } from "../../../../../types/optionField";
const { useEffect, useState } = React;

const CKEditor: React.FC<ICkEditor> = ({
  config,
  type,
  style,
  name,
  nameFields,
  pressedID,
  viewID,
  content,
  setContent,
  initContent,
  dispatchEvent,
  parameterOrField,
  fieldId,
  setHtmlFields,
  view_name,
  setJsonField,
  setJsonParam
}) => {
  const [element, setElement] = useState<HTMLDivElement | null>(null);
  const { editor, status } = useCKEditor({
    config,
    element,
    dispatchEvent,
    type,
  });
  const [editorReady, setEditorReady] = useState<boolean>(false);
  const { id } = useParams();
  const [otherPress, setOtherPress] = useState(false);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    setContent(nameFields);
    setOtherPress(!otherPress);
  }, [pressedID]);

  useEffect(() => {
    if (editor && status === "ready") {
      if (config) {
        if (editor && editor.status) {
          let element = editor.document.createElement("span");
          if (!parameterOrField) {
            element.setAttribute("ds_field_id", fieldId);
            element.setAttribute("ds_id", viewID);
            element.setAttribute("ds_field_name", nameFields);
            element.setAttribute("ds_name", view_name);
          }
          element.setAttribute("template_id", id);
          editor.insertElement(element);
          if (content != "") {
            element.setText(`[${content}$]`);
          }
          setEditorReady(true);
          let arrSpans: any;
          arrSpans = editor.document.$.body.getElementsByTagName("span");
          tmpId = arrSpans[0].getAttribute("template_id");
          for (let item of arrSpans) {
            let html = item.innerHTML;
            if (item.getAttribute("ds_id") != null) {
              var arrAtributes = item.getAttributeNames();
              const before_ = html.substring(1, html.indexOf("$"));
              let obj: IOptionField = {
                template_id: 0,
                ds_id: 0,
                ds_field_name: before_,
                ds_field_id: 0,
                ds_name: view_name,
              };
              let a: keyof IOptionField;
              for (a of arrAtributes) {
                (obj[a] as any) = item.getAttribute(a);
              }
              jsonField.push(obj);
            }
            else {
              var arrAtributes = item.getAttributeNames();
              const before_ = html.substring(1, html.indexOf("$"));
              let obj: IOptionParam = {
                template_id: 0,
                parameter_name: ""              
              };
              let a: keyof IOptionParam;
              for (a of arrAtributes) {
                (obj[a] as any) = item.getAttribute(a);
              }
              obj["parameter_name"] = before_;
              jsonParam.push(obj);
            }
          }
          setHtmlFields && setHtmlFields(jsonField);
          setJsonField(jsonField);
          setJsonParam(jsonParam);
        }
      }
    }
  }, [otherPress]);

  useEffect(() => {
    if (editor && status === "ready") {
      editor.container.setStyles(style);
      let fragment = document
        .createRange()
        .createContextualFragment(initContent);
      editor.document.$.body.appendChild(fragment);
    }
  }, [editor, status]);
  
  let jsonField: Array<IOptionField> = [];
  let jsonParam: Array<IOptionParam> = [];
  let tmpId: number = 0;

  return (
    <div>
      <Button
        label="מקור"
        icon="pi pi-external-link"
        onClick={() => {
          setVisible(true);
        }}
      />
      <Dialog
        header="Header"
        visible={visible}
        style={{ width: "50vw", height: "20vm" }}
        onHide={() => setVisible(false)}
      >
        <p className="m-0">
          {editor &&
            status === "ready" &&
            editor.document.$.body.innerHTML &&
            editor.document.$.body.innerHTML}
        </p>
      </Dialog>
      <div
        id={name}
        style={status !== "ready" ? { visibility: "hidden" } : {}}
        ref={setElement}
      ></div>
    </div>
  );
};

export default CKEditor;

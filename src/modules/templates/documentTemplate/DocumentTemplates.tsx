import React, { useEffect, useState } from "react";
import DefinitionTables from "./menus/DefinitionTables";
import { TabMenu } from "primereact/tabmenu";
import TemplateDetails from "./menus/TemplateDetails";
import { useLocation, useParams } from 'react-router-dom';
import TemplateViews from "./menus/TemplateViews";

const DocumentTemplates = () => {

  let { id } = useParams();
  const [isComplete, setIsComplete] = useState<boolean>(true)
  const [currentComponent, setCurrentComponent] = useState<any>(<TemplateDetails templateId={id} setIsComplete={setIsComplete} />);
  const location = useLocation();
  const activeTabState = location?.state?.data; // שלוף מידע על הכרטיסיה הפעילה מהנתיב
  const [activeTabIndex, setActiveTabIndex] = useState(activeTabState || 0);


  useEffect(() => {
    debugger
    if (location.state && location.state.data === 1) {
      setActiveTabIndex(1); // Update to tab 1 if the state data is 1
    } else {
      setActiveTabIndex(0); // Update to tab 0 if the state data is not 1
    }
  }, [location, location.state]);

  const setView = () => {
    if (id) {
      console.log(id, { id })
      setActiveTabIndex(1)
      setCurrentComponent(<TemplateViews templateId={id} setIsComplete={setIsComplete} />)
    }
    else {
      setCurrentComponent(<TemplateDetails templateId={id} setIsComplete={setIsComplete} />); // Reset currentComponent if id is not present
    }
  }

  useEffect(() => {
    setView()
  }, [id])


  useEffect(() => {
    console.log(activeTabIndex, "activTab")
  })

  const items = [
    {
      label: 'פרטי התבנית',
      icon: 'pi pi-spin pi-cog',
      command: (() => {
        setCurrentComponent(<TemplateDetails currentComponent={currentComponent} setCurrentComponent={setCurrentComponent} templateId={id} setIsComplete={setIsComplete} />)
          ; setActiveTabIndex(0)
      })
    },
    {
      disabled: false,
      label: 'עיצוב התבנית',
      icon: 'pi pi-search',
      command: () => {
        setCurrentComponent(<TemplateViews templateId={id} />);
        setActiveTabIndex(1)
      }
    },
    {
      disabled: false,
      label: 'הגדרת טבלאות',
      icon: 'pi pi-user',
      command: () => {
        setCurrentComponent(<DefinitionTables templateId={id} />);
        setActiveTabIndex(2)
      },
    }
  ]

  return (
    <div>
      <TabMenu model={items} activeIndex={activeTabIndex} />
      <div> {currentComponent}</div>
    </div>
  );
}

export default DocumentTemplates;

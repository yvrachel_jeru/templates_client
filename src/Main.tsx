import React from 'react';
import { Route, Routes } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import Home from './pages/Home';
import myRoutes from './pages/routes';


const routers = myRoutes.map((r, index) => (
  <Route key={index} path={r.path} element={r.component}></Route>
));

const Main = () => {
  return (
    <div>
      <Home> <BrowserRouter>
        <Routes>{routers}</Routes>
      </BrowserRouter></Home>
    </div>
  );
};

export default Main;
//views components include grid,add ,update view and filter
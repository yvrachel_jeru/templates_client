const dev = {
    url: {
      API: "https://localhost:44384/api/",
    }
  };
  const prod = {
    url: {
      API: "https://rishuybniyaonlineng.jerusalem.muni.il/RboApi/api/",

    },
  };
  
  export const config = process.env.NODE_ENV === "development" ? dev : prod;
  
  
export const constants = {
    Controllers: {
      GetJson: "Db/ExecuteGetJSON",
      GetSelect: "Db/ExecuteSelect",
      GetScalar: "Db/ExecuteChange",
    },
    luTable: {
      MASLUL: 2092,
    },
    systems: {
      Rishui: 26400046,
    },

}
import { IBody, IParams } from "../../types/api";
import { post } from "../data/api";

let paramToSend: IParams[] = [
];

let params: IBody = {
    params: paramToSend,
    storedProcedure: "templates_save_template",
    scheme: "",
    columns: [""],
};

const saveHtml = (param: IParams[]) => {
    
    param.forEach(e => {
        paramToSend.push(e);
    });
    console.log(paramToSend);
    
    post("Data/GetData", params)
    .then(() => {
    
      paramToSend = [];
    })
    .catch(error => {
      // Handle any errors that occur during the request
      console.error(error);
    });
};


const saveParameters = (param: IParams[]) => {
    console.log(param);
    param.forEach(e => {
        paramToSend.push(e);
    });
};
const saveFields = (param: IParams[]) => {
    console.log(param);
    param.forEach(e => {
        paramToSend.push(e);
    });
};
const savePhotoPath = (param: IParams[]) => {
    console.log(param);
    param.forEach(e => {
        paramToSend.push(e);
    });
};

const SaveTemplateService = {
    saveHtml,
    saveParameters,
    savePhotoPath,
    saveFields,
}

export default SaveTemplateService;
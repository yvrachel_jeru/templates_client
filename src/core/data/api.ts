import { IBody } from "../../types/api";
import axios from "axios";
import { config } from "../config";
import idForPath from "../../types/idForPath";
import { IParamsForPreview } from "../../types/paramsForPreview";
import Swal from "sweetalert2";


export async function post(endpoint: string, params: IBody) {
  try {
    const url = config.url.API
    const { data } = await axios.post(url + endpoint, params, {
      withCredentials: true,
    });
    return await data;
  }
  catch (error) {
  
  }
}

export interface htmlJson {
  templateId?: number,
  templateName: string,
  htmlText: string
}



export async function template(endpoint: string, params: htmlJson) {
  try {
    const url = config.url.API
    const { data } = await axios.post(url + endpoint, params, {
      withCredentials: true,
    });
    return await data;
  }
  catch (error) {
    Swal.fire({
      icon: 'warning',
      title: 'Warning',
      text: 'הטיוטה לא נשמרה, נסה שנית',
      showCancelButton: false,
      confirmButtonColor: '#0000FF',
      confirmButtonText: 'OK'
    });  }
}

export async function getHTML(endpoint: string, params: idForPath) {
  try {
    const url = config.url.API
    const { data } = await axios.post(url + endpoint, params, {
      withCredentials: true,
    });
    return await data;
  }
  catch (error) {
    console.log(error);
  }
}


export async function getPreview(endpoint: string, params: IParamsForPreview) {
  try {
    const url = config.url.API
    const { data } = await axios.post(url + endpoint, params, {
      withCredentials: true,
    });
   
    return await data;
  }
  catch (error) {
    console.log(error);
  }
}

export async function deletePreview(endpoint: string, previewUrl: string) {
  try {
    const url = config.url.API;
    const { data } = await axios.post(`${url}${endpoint}?fileUrl=${encodeURIComponent(previewUrl)}`);
    return data;
  } catch (error) {
    console.log(error);
  }
}
import { IBody, IParams} from "../types/api";
import { FilterType, GridSource } from "../types/gridSource";
import { DataTable } from "primereact/datatable";
import { useEffect, useState } from "react";
import { Column } from "primereact/column";
import { post } from "../core/data/api";
import { Button } from "primereact/button";
import React from "react";
import { Dialog } from "primereact/dialog";
import { useNavigate } from "react-router";
import MainGrid from "../components/grid";
import Swal from 'sweetalert2';
import ITemplate from "../types/template";
import { ToastContainer, toast } from 'react-toastify';

export default function Templates() {
    const [dataItems, setDataItems] = useState();
    const [globalFilter] = useState(null);
    const navigate = useNavigate();
    const [selectedTemplate, setSelectedTemplate] = useState();
    const [deleteChoosen, setDeleteChoosen] = useState(0);
    const [copyChoosen, setCopyChoosen] = useState(0);
    const [visible, setVisible] = useState(true);
    const [submitted, setSubmitted] = useState<boolean>(false);
    const [productDialog, setProductDialog] = useState(false);

    useEffect(() => {
        getTemplates();
    }, []);

    useEffect(() => {
        if (selectedTemplate !== undefined)
            editProduct(selectedTemplate);
    }, [selectedTemplate])

    useEffect(() => {
        if (deleteChoosen !== 0)
            dropTemplate(deleteChoosen)
        getTemplates()
    }, [deleteChoosen])

    useEffect(() => {
        if (copyChoosen !== 0)
            duplicateTemplate(copyChoosen)
        getTemplates()
    }, [copyChoosen])

    const params: IBody = {
        params: [
            {
                parameterName: "creator_user",
                direction: 1,
                dbType: 1,
                value: "tamar&nechami",
                isTableValue: false
            }
        ],
        storedProcedure: "template_getAllTemplates",
        scheme: "",
        columns: [
            ""
        ]
    }

    const params2: IBody = {
        params: [
            {
                parameterName: "template_id",
                direction: 1,
                dbType: 1,
                value: JSON.stringify(deleteChoosen),
                isTableValue: false
            }
        ],
        storedProcedure: "template_dropTemplate",
        scheme: "",
        columns: [
            ""
        ]
    }

    const getTemplates = () => {
        post("Data/GetData", params).then(data => { 
            if (data && data.data && data.data[0]) {
                correctDate(data.data[0]);
                setDataItems(data.data[0]);
            } else {
                console.error("No connection to the server or empty data");
            }
        })
    }
    

    const hideDialog = () => {
        setSubmitted(false);
        setProductDialog(false);
    }

    const saveField = () => {
      
        setSubmitted(false);
        setProductDialog(false);
        post("Data/GetData", params2)
        .then(() => {
            // After deletion is successful, retrieve all templates by calling getTemplates()
            getTemplates();
        });
    }

    const footerContent = (
        <React.Fragment>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="DeleteTemplate" icon="pi pi-check" className="p-button-text" onClick={saveField} />
        </React.Fragment>
    )

    const dropTemplate = (id: any) => {
        Swal.fire({
            title: "",
            text: "האם אתה בטוח שברצונך למחוק את התבנית?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#FFA500",
            cancelButtonColor: "#0000FF",
            confirmButtonText: "מחק",
            preConfirm: () => {
                saveField()
            },
            cancelButtonText: "ביטול",
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !Swal.isLoading(),
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    title: "",
                    text: "התבנית נמחקה בהצלחה",
                    icon: "success"
                });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // Function to handle the action when the cancel button is clicked
                // You can add your custom logic here
            }
        });
    }

    const fieldsArr: GridSource[] = [
        {
            field: "template_id",
            header: "מזהה תבנית",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי מזהה ",
            filterType: FilterType.number
        },
        {
            field: "template_name",
            header: "שם תבנית",
            isEditable: true,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string
        },
        {
            field: "system_name",
            header: "שם מערכת",
            isEditable: true,
            filterPlaceholder: "חיפוש לפי שם",
            filterType: FilterType.string
        },
        {
            field: "doc_name",
            header: "שם מסמך",
            isEditable: true,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string
        },
        {
            field: "template_path",
            header: "ניתוב מסמך",
            isEditable: true,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string
        },
        {
            field: "last_modify_date",
            header: "תאריך עדכון אחרון",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי תאריך ",
            filterType: FilterType.string
        },
        {
            field: "last_modify_user",
            header: "משתמש מעדכן אחרון",
            isEditable: false,
            filterPlaceholder: "חיפוש לפי שם ",
            filterType: FilterType.string
        },
    ]


    const columns: any = fieldsArr.map((element: GridSource) => {
        return <Column key={element.field} field={element.field} header={element.header} filter filterPlaceholder={element.filterPlaceholder} filterMatchMode="contains" sortable />
    })


    const add = () => {
        navigate("/templates/DocumentTemplates/")
    }

    const actionBodyTemplate = (rowData: any) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-pencil" rounded text severity="secondary" aria-label="Bookmark" onClick={() => setSelectedTemplate(rowData.template_id)} />
                <Button icon="pi pi-trash" rounded text severity="secondary" aria-label="Bookmark" onClick={() => setDeleteChoosen(rowData.template_id)} />
                <Button icon="pi pi-copy" rounded text severity="secondary" aria-label="Bookmark" onClick={() => setCopyChoosen(rowData.template_id)} />
            </React.Fragment>
        );
    }

    const duplicateTemplate = (id : number) => {
        try{
            const idParams: IParams ={
                parameterName: "template_id",
                direction: 1,
                dbType: 1,
                //value: id,
                value: JSON.stringify(copyChoosen),
                isTableValue: false
            }
            const tBody: IBody = {
                params: [idParams],
                storedProcedure: "template_duplicate",
                scheme: "",
                columns: []
            }
            post("Data/GetData", tBody)
            //.then(data => (navigate(`/templates/DocumentTemplates/${(data.data[0][0].template_id)}`)))
            .then(() => {
            getTemplates()   
        }); 
        }
        catch(error) {
            console.error("שגיאה בהעתקת התבנית:", error);
            toast.error("אירעה שגיאה בעת העתקת התבנית. אנא נסה שוב מאוחר יותר.");
            return
        };
        toast.success("התבנית הועתקה בהצלחה");
    }

    const editProduct = (product: any) => {
        console.log("product", product)
        navigate(`/templates/DocumentTemplates/${product}`)
    }

    const correctDate = (data: any) => {
        data.forEach((e: any) => {
            const datetimeFromSql2 = e.last_modify_date;
            const datetime2 = new Date(datetimeFromSql2);
            e.last_modify_date = datetime2.toLocaleDateString();
        });
        setDataItems(data);
    }


    const mainGridProps = {
        data: dataItems,
        fieldsArr: fieldsArr,
        implementsEdit: true,
        editFunc: editProduct,
        isNavigable: true,
        navigate: '/templates/DocumentTemplates',
        mainField: 'template_id',
        jsxElement: actionBodyTemplate
    };
    const [isBlue, setIsBlue] = useState(true);

    const handleButtonClick = () => {
        setIsBlue(!isBlue);
        add();
    };
    const buttonStyle = {
        border: `2px solid ${isBlue ? 'blue' : 'orange'}`,
    };


    return (
        <div className="datatable-filter-demo">
            <ToastContainer />
            <div className="card">
                <Button
                    label=" תבנית חדשה"
                    style={buttonStyle}
                    onClick={handleButtonClick}
                />
                <br>
                </br>
                <br>
                </br>
                <MainGrid {...mainGridProps}></MainGrid>
               
            </div>
        </div>
        // <div className="datatable-filter-demo">
        //     <ToastContainer />
        //     <div className="card">
        //         <Button onClick={() => { add() }}>הוסף</Button>
        //         <DataTable dataKey="selectedTemplate" editMode="row" className="p-datatable" value={dataItems} paginator rows={10} filterDisplay="row"
        //             globalFilter={globalFilter} >
        //             {columns}
        //             <Column body={actionBodyTemplate}></Column>
        //             <Button label="Show" icon="pi pi-external-link" onClick={() => setVisible(true)} />
        //         </DataTable>
        //         {submitted && (
        //             <Dialog header="האם אתה בטוח שברצונך למחוק את התבנית?" visible={visible} style={{ height: '10vw', width: '10vw' }} onHide={() => setVisible(false)} footer={footerContent} >
        //                 <p className="m-0">
        //                 </p>
        //             </Dialog>)}
        //     </div>
        // </div>
    );
}

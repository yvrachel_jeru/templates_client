import { Menubar } from "primereact/menubar";
import React from "react";
import tabsItems from "./tabsItems";

const Home = (props: any) => {
  const [sidebarIsOpened, setsidebarIsOpened] = React.useState<boolean>(true);
  const [demoUser, setDemoUser] = React.useState<string>("רחל יעבץ");

  const start = (
    <button type="button" onClick={() => setsidebarIsOpened(true)}>
      <i className="pi pi-bars"></i>
    </button>
  );
  
  const end = (
    <ul className="end-menu">
      <li className="menu-item">
        <span className="menu-item__text search">
          <i className="pi pi-search"></i>&nbsp;חפש
        </span>
      </li>
      <li className="menu-item">
        <span className="menu-item__text">
          <i className="pi pi-user"></i>&nbsp;{demoUser}
        </span>
      </li>
    </ul>
  );

  return (
    <>
      <div className="main-page">
        <div className="main-page__toolbar">
          <div className="container">
            <Menubar model={tabsItems.items}  end={end}></Menubar>
          </div>
        </div>
        <div className="container">{props.children}</div>
      </div>
    </>
  );
};

export default Home;

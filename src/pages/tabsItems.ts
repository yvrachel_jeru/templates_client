export default {
    items: [
        { label: 'דף הבית', icon: 'pi pi-fw pi-home', url: '/home' },
        {
            label: 'ניהול תבניות',
            icon: 'pi pi-fw pi-file',
            items: [
                {
                    label: 'תבניות מסמכים',
                    icon: 'pi pi-spin pi-cog',
                    url: '/templates/'
                },
                {
                    label: 'טיוטות',
                    icon: 'pi pi-search',
                    url: '/templates/drafts'
                },
                {
                    label: 'סל המיחזור',
                    icon: 'pi pi-user',
                    url: '/templates/recycleBin'
                }
            ]
        },
        {
        label: 'ניתוב למערכות',
        icon: 'pi pi-fw pi-file',
        items: [
            {
                label: 'מערכות קיימות',
                icon: 'pi pi-spin pi-cog',
                url: '/infrastructures/existingSystems'
            },
            {
                label: 'תצוגות למערכת',
                icon: 'pi pi-search',
                url: '/infrastructures/viewsSystem'
            },
            // {
            //     label: 'שדות לתצוגה',
            //     icon: 'pi pi-user',
            //     url: '/infrastructures/displayFields'
            // },
            {
                label: 'בקרת נתונים',
                icon: 'pi pi-search',
                url: '/infrastructures/dataControl'
            },
            {
                label: 'לוג השגיאות',
                icon: 'pi pi-user',
                url: '/infrastructures/errorLog'
            }
        ]}
    ]
}
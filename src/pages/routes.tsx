import React from "react"
import { Iroute } from "../types/route"
import Infrastructures from "../pages/Infrastructures"
import Templates from "./Templates"
import Drafts from "../modules/templates/Drafts"
import RecycleBin from "../modules/templates/RecycleBin"
import DataControl from "../modules/infrastructures/DataControl"
import ErrorLog from "../modules/infrastructures/ErrorLog"
import ExistingSystems from "../modules/infrastructures/systems/ExistingSystems"
import DocumentTemplates from "../modules/templates/documentTemplate/DocumentTemplates"
import DefinitionTables from "../modules/templates/documentTemplate/menus/DefinitionTables"
import TemplateViews from "../modules/templates/documentTemplate/menus/TemplateViews"
import Home from "./Home"
import TemplateDetails from "../modules/templates/documentTemplate/menus/TemplateDetails"
import ViewsSystem from "../modules/infrastructures/views/ViewsSystem"
import DisplayFields from "../modules/infrastructures/Fields/DisplayFields"

const myRoutes: Array<Iroute> = [
  {
    path: "/",
    name: "home",
    layout: "/rtl",
    component: <Home />,
  },
  {
    path: "/templates",
    name: "templates",
    component: < Templates />,
    layout: "/rtl"
  },
  {
    path: "/templates/DocumentTemplates",
    name: "DocumentTemplates",
    component: < DocumentTemplates />,
    layout: "/rtl"
  },
  {
    path: "/templates/DocumentTemplates/:id",
    name: "DocumentTemplates",
    component: < DocumentTemplates />,
    layout: "/rtl"
  },
  {
    path: "/templates/drafts",
    name: "drafts",
    component: <Drafts />,
    layout: "/rtl",
  },
  {
    path: "/templates/recycleBin",
    name: "recycleBin",
    component: <RecycleBin />,
    layout: "/rtl",
  },
  {
    path: "/infrastructures",
    name: "infrastructures",
    component: <Infrastructures />,
    layout: "/rtl"
  },
  {
    path: "/infrastructures/dataControl",
    name: "dataControl",
    component: <DataControl />,
    layout: "/rtl",
  },
  {
    path: "/infrastructures/displayFields/:id/:systemId",
    name: "displayFieldsByViewId",
    component: <DisplayFields />,
    layout: "/rtl",
  },
  {
    path: "/infrastructures/errorLog",
    name: "errorLog",
    component: <ErrorLog />,
    layout: "/rtl",
  },
  {
    path: "/infrastructures/existingSystems",
    name: "existingSystems",
    component: <ExistingSystems />,
    layout: "/rtl",
  },
  {
    path: "/infrastructures/viewsSystem",
    name: "viewsSystem",
    component: <ViewsSystem />,
    layout: "/rtl",
  },
  {
    path: "/infrastructures/viewsSystem/:id",
    name: "viewsSystemBySystemId",
    component: <ViewsSystem />,
    layout: "/rtl",
  },
  {
    path: "/templates/documentTemplates/templateDetails/:id",
    name: "templateDetails",
    component: < TemplateDetails />,
    layout: "/rtl",
  },

]
export default myRoutes;
